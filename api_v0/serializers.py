import pickle
from django.db import transaction
from django.shortcuts import get_object_or_404
from rest_framework import serializers
from tasks.models import MathTask, RelationFormTemplate, Problem, Relation, RelationArg
from solver.reasoning import Logger


class MathTaskDefaultSerializer(serializers.ModelSerializer):
    class Meta:
        model = MathTask
        fields = ('created', 'relations', 'to_derive', 'to_calculate', )

    relations = serializers.SerializerMethodField()
    to_derive = serializers.SerializerMethodField()
    to_calculate = serializers.SerializerMethodField()

    def get_relations(self, obj):
        relations = pickle.loads(obj.relations) if obj.relations else []
        return [Logger.to_unicode(r) for r in relations]

    def get_to_derive(self, obj):
        to_derive = pickle.loads(obj.to_derive) if obj.to_derive else []
        return [Logger.to_unicode(r) for r in to_derive]
    
    def get_to_calculate(self, obj):
        to_calculate = pickle.loads(obj.to_calculate) if obj.to_calculate else []
        return [Logger.to_unicode(r) for r in to_calculate]


class RelationFormTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = RelationFormTemplate
        fields = ('id', 'template', 'description', )


class RelationArgSerializer(serializers.ModelSerializer):
    class Meta:
        model = RelationArg
        fields = ('name', 'position')


class RelationSerializer(serializers.ModelSerializer):
    args = RelationArgSerializer(many=True)

    class Meta:
        model = Relation
        fields = ('template', 'args', )


class ProblemListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Problem
        fields = ('id', 'title', 'created', )


class ProblemSerializer(serializers.ModelSerializer):
    relations = RelationSerializer(many=True)
    relations_to_derive = RelationSerializer(many=True)

    class Meta:
        model = Problem
        fields = ('id', 'title', 'created', 'relations', 'relations_to_derive', )

    def create(self, validated_data):
        relations_data = validated_data.pop('relations')
        relations_to_derive_data = validated_data.pop('relations_to_derive')

        with transaction.atomic():
            problem = Problem.objects.create(**validated_data)

            self.create_relations(problem, relations_data, Relation.GIVEN)
            self.create_relations(problem, relations_to_derive_data, Relation.TO_DERIVE)

        return problem

    def update(self, problem, validated_data):
        relations_data = validated_data.pop('relations')
        relations_to_derive_data = validated_data.pop('relations_to_derive')

        super(ProblemSerializer, self).update(problem, validated_data)

        with transaction.atomic():
            Relation.objects.filter(problem=problem).delete()

            self.create_relations(problem, relations_data, Relation.GIVEN)
            self.create_relations(problem, relations_to_derive_data, Relation.TO_DERIVE)

        return problem

    def create_relations(self, problem, relations_data, relation_type):
        for relation_data in relations_data:
            args_data = relation_data.pop('args')
            relation = Relation.objects.create(problem=problem, type=relation_type, **relation_data)

            for arg_data in args_data:
                arg = RelationArg.objects.create(relation=relation, **arg_data)
