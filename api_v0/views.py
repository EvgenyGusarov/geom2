from django.shortcuts import render
from rest_framework import generics
from tasks.models import MathTask, RelationFormTemplate, Problem, Relation, RelationArg
from .serializers import (MathTaskDefaultSerializer, RelationFormTemplateSerializer,
                          ProblemListSerializer, ProblemSerializer)


class MathTaskListApiView(generics.ListAPIView):
    serializer_class = MathTaskDefaultSerializer

    def get_queryset(self):
        return MathTask.objects.order_by('-created')


class RelationFormTemplateView(generics.ListAPIView):
    serializer_class = RelationFormTemplateSerializer
    queryset = RelationFormTemplate.objects.all()


class MathTaskDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = MathTask.objects.all()
    lookup_url_kwarg = 'task_id'


class ProblemListApiView(generics.ListAPIView):
    serializer_class = ProblemListSerializer
    queryset = Problem.objects.all().order_by('-created')


class ProblemDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Problem.objects.all()
    lookup_url_kwarg = 'task_id'
    serializer_class = ProblemSerializer


class ProblemCreateView(generics.CreateAPIView):
    serializer_class = ProblemSerializer
