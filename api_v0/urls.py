from django.conf.urls import url, include
from .views import (MathTaskListApiView, RelationFormTemplateView, MathTaskDetailView,
                    ProblemListApiView, ProblemDetailView, ProblemCreateView)

urlpatterns = [
    # url(r'^task/$', MathTaskListApiView.as_view()),
    # url(r'^task/(?P<task_id>)[0-9]+/$', MathTaskDetailView.as_view()),
    # url(r'^task/relation-template/$', RelationFormTemplateView.as_view()),

    url(r'^task/$', ProblemListApiView.as_view()),
    url(r'^task/(?P<task_id>[0-9]+)/$', ProblemDetailView.as_view()),
    url(r'^task/relation-template/$', RelationFormTemplateView.as_view()),
    url(r'^task/new/$', ProblemCreateView.as_view()),
]
