# coding: utf-8

from __future__ import unicode_literals

import operator
import os
import itertools
from collections import defaultdict
from copy import deepcopy
from contextlib import contextmanager
from jinja2 import Template

import objects
from objects import get_effective_type, check_args_are_of_type, CompositeObject, ComplexName, ExpressionParser
import statements
from statements import LogicStatement, Relation, ExistsFactory, FunctionCall, EqualityRelation, \
    AlgebraicEqualityRelation, ConstAlgebraicExpression, AlgebraicExpression, Variable, LinearEquationSolver


class NoSatisfyingAliases(Exception):
    pass


class CanNotDerive(Exception):
    pass


class CanNotJustify(Exception):
    pass


class Logger(object):
    def __init__(self):
        self.buffer = []
        self.buffer_is_on = False

    def append(self, log_str):
        self.buffer.append(log_str)

    def extend(self, items):
        self.buffer.extend(self.flatten_list(items))

    @staticmethod
    def list_to_unicode(arg):
        return u'[' + u', '.join(map(Logger.to_unicode, arg)) + u']'

    @staticmethod
    def dict_to_unicode(arg):
        return '{' + \
               ', '.join(
                   '{} -> {}'.format(Logger.to_unicode(key), Logger.to_unicode(value))
                   for key, value in arg.iteritems()
               ) + '}'

    @staticmethod
    def to_unicode(arg):
        if isinstance(arg, (list, tuple)):
            return Logger.list_to_unicode(arg)
        elif isinstance(arg, dict):
            return Logger.dict_to_unicode(arg)
        else:
            return unicode(arg)

    @staticmethod
    def flatten_list(items):
        res = []
        bar = False

        for index, item in enumerate(items):
            if isinstance(item, (list, tuple)):
                if index != 0 and not bar:
                    res.append('')
                    bar = True
                res.extend(Logger.flatten_list(item))
                if index != len(items) - 1:
                    res.append('')
                    bar = True
            elif isinstance(item, basestring):
                res.append(item)
                bar = False
            else:
                print 'unexpected item {}'.format(item)

        return res
        
    def flush(self):
        print u'\n'.join(map(unicode, self.buffer))
        self.clear()

    def clear(self):
        self.buffer = []

    def render(self, template_name, **context):
        template_path = os.path.join(os.getcwd(), 'templates', template_name)
        with open(template_path, 'rb') as template_file:
            template = Template(template_file.read())

        return template.render(**context)

    def render_node(self, node, children, parent=None):
        if node.type == ReasoningNode.Types.FINAL:
            template_name = 'final.html'
        elif node.type == ReasoningNode.Types.DATA:
            template_name = 'data.html'
        elif node.type == ReasoningNode.Types.LOGIC:
            template_name = 'logic.html'
        elif node.type == ReasoningNode.Types.TRANSFORMATION:
            template_name = 'transformation.html'
        elif node.type == ReasoningNode.Types.EQUATION:
            template_name = 'equation.html'
        else:
            raise Logger.TemplateNotFound

        context = {
            'statement': node.relation,
            'justifications': node.justifications,
            'transformations': node.transformations,
            'theorem': node.theorem,
            'args': node.args,
            'children': [node.relation for node in children]
        }

        if parent:
            context['parent_statement'] = parent.relation

        template_path = os.path.join(os.getcwd(), 'templates', template_name)
        with open(template_path, 'rb') as template_file:
            template = Template(template_file.read())

        return template.render(**context)

    class TemplateNotFound(Exception):
        pass


class Justifier(object):
    def __init__(self, reasoning):
        self.reasoning = reasoning

    def can_approve(self, relation):
        try:
            self.get_justifications(relation)
            return True
        except CanNotJustify:
            return False

    def get_justifications(self, relation):
        raise NotImplementedError


class NoObjectsOfType(Exception):
    pass


class ObjectsManager(object):
    def __init__(self, reasoning):
        self.registry = defaultdict(lambda: [])
        self.reasoning = reasoning

    def add_object(self, obj):
        obj_type = get_effective_type(obj)
        if obj not in self.registry[obj_type]:
            self.registry[obj_type].append(obj)
            self.reasoning.equality_manager.add(obj)

    def add_object_and_dependencies(self, obj):
        obj_type = get_effective_type(obj)
        if obj in self.registry[obj_type]:
            return

        self.add_object(obj)

        for dependency in obj.dependencies:
            self.add_object(dependency)

        if isinstance(obj, objects.Segment):
            self.reasoning.layout.handle_new_segment(obj)

        self.reasoning.equality_manager.simplify_object(obj)

    def add_from_statement(self, statement):
        if isinstance(statement, Relation):
            for obj in statement.args:
                self.add_object_and_dependencies(obj)
            if isinstance(statement, statements.PointOnSegment):
                self.reasoning.layout.handle_point_on_segment(statement)
        elif isinstance(statement, LogicStatement):
            all_args = sum(statement.walk(lambda x: x.args), [])
            for obj in all_args:
                self.add_object_and_dependencies(obj)

            if statement.logic_op in ('exists', 'for_each'):
                for obj in statement.quantor_args:
                    self.add_object_and_dependencies(obj)
        elif isinstance(statement, ExistsFactory):
            for obj in statement.build_consequence_factory_args(*statement.sample_args):
                self.add_object_and_dependencies(obj)

    def iter_tuples_of_type(self, types):
        all_items = []
        for arg_type in types:
            items_of_type = []
            for type_key in self.registry:
                if issubclass(type_key, arg_type):
                    items_of_type.extend(self.registry[type_key])
            if items_of_type:
                all_items.append(items_of_type)
            else:
                raise NoObjectsOfType('No objects of type {}'.format(arg_type.type_verbose))

        return itertools.product(*all_items)

    def find_by_point(self, point):
        res = defaultdict(lambda: [])
        classes = [objects.Segment, objects.Angle, objects.Triangle, ]

        for key in classes:
            for obj in self.registry[key]:
                if point in obj.atoms:
                    res[key].append(obj)

        return res

    def pick_unique_names(self, args):
        res = []

        for arg in args[:]:
            if isinstance(arg, (FunctionCall, CompositeObject)):
                res.append(arg)
            else:
                if isinstance(arg.name, basestring):
                    name = ComplexName(arg.name)
                elif isinstance(arg.name, ComplexName):
                    name = arg.name

                while 1:
                    collision = False

                    # print 'try name {} for {}'.format(name, arg)
                    for obj in self.registry[type(arg)]:
                        if unicode(obj.name) == unicode(name):
                            # print 'collision'
                            collision = True
                            break
                    if collision:
                        name = name.next()
                        continue
                    else:
                        # Avoid renaming sample_args
                        arg = deepcopy(arg)
                        arg.name = name
                        res.append(arg)
                        break

        return res

    def __contains__(self, item):
        arg_type = get_effective_type(item)
        return item in self.registry.get(arg_type, [])

    def __unicode__(self):
        return ', '.join(
            '{}: {}'.format(
                arg_type.type_verbose,
                Logger.list_to_unicode(self.registry[arg_type])
            ) for arg_type in self.registry
        )


# class GraphNode(object):
#     def __init__(self, obj, index):
#         self.obj = obj
#         self.index = index
#


class GraphEdge(object):
    def __init__(self, start_index, end_index):
        self.start_index = start_index
        self.end_index = end_index


class Graph(object):
    edge_class = GraphEdge

    def __init__(self):
        self.nodes = []
        self.edges = defaultdict(lambda: {})

    def add_node(self, node):
        self.nodes.append(node)

    def has_node(self, node):
        return node in self.nodes

    def has_edge(self, node_1, node_2):
        index_1 = self.nodes.index(node_1)
        index_2 = self.nodes.index(node_2)

        return index_2 in self.edges[index_1]

    def get_edge(self, node_1, node_2):
        index_1 = self.nodes.index(node_1)
        index_2 = self.nodes.index(node_2)

        return self.edges[index_1].get(index_2)

    def has_edges(self, start, ends):
        for end in ends:
            if not self.has_edge(start, end):
                return False
        return True

    def get_edge_start(self, edge):
        return self.nodes[edge.start_index]

    def get_edge_end(self, edge):
        return self.nodes[edge.end_index]

    def build_edge(self, start_obj, end_obj):
        for obj in (start_obj, end_obj):
            if obj not in self.nodes:
                self.nodes.append(obj)

        start_index = self.nodes.index(start_obj)
        end_index = self.nodes.index(end_obj)
        return self.edge_class(start_index, end_index)

    def add_edge(self, edge):
        start_index = edge.start_index
        end_index = edge.end_index

        self.edges[start_index][end_index] = edge
        self.edges[end_index][start_index] = edge

    def create_edge(self, start_obj, end_obj):
        edge = self.build_edge(start_obj, end_obj)
        self.add_edge(edge)

    def collect_connected_node_indices(self, obj):
        if not self.has_node(obj):
            return

        connected_node_indices = [self.nodes.index(obj)]
        cursor = 0

        while True:
            next_cursor = len(connected_node_indices)
            if cursor >= next_cursor:
                break
            for start_index in connected_node_indices[cursor:]:
                for end_index in self.edges[start_index]:
                    if end_index not in connected_node_indices:
                        connected_node_indices.append(end_index)
            cursor = next_cursor

        return connected_node_indices

    def collect_connected_nodes(self, obj):
        connected_node_indices = self.collect_connected_node_indices(obj)
        return [self.nodes[index] for index in connected_node_indices]

    def split_to_connected_component_indices(self):
        components = []
        splitted_nodes = set()

        for index, obj in enumerate(self.nodes):
            if index in splitted_nodes:
                continue

            group = self.collect_connected_node_indices(obj)
            components.append(group)
            splitted_nodes.update(group)

        # print 'components', Logger.to_unicode(components)
        return components

    def split_to_connected_components(self):
        components = self.split_to_connected_component_indices()
        # print 'component indices', components
        res = []
        for component in components:
            # print 'component objects', [self.nodes[index] for index in component]
            res.append([self.nodes[index] for index in component])
        return res

    def find_path_indices(self, start_index, end_index):
        paths = [[start_index]]

        while True:
            new_paths = []

            for path in paths:
                for next_index in self.edges[path[-1]]:
                    if next_index == end_index:
                        return path + [next_index]
                    if next_index in path:
                        continue
                    new_paths.append(path + [next_index])

            if new_paths:
                paths = new_paths
            else:
                break

    def find_path(self, start_obj, end_obj):
        if not self.has_node(start_obj) or not self.has_node(end_obj):
            return

        start_index = self.nodes.index(start_obj)
        end_index = self.nodes.index(end_obj)

        path_indices = self.find_path_indices(start_index, end_index)
        if path_indices is None:
            return

        path = [self.nodes[index] for index in path_indices]
        return path

    def convert_path_indices_to_edges(self, path_indices):
        edges = []
        for index, next_index in zip(path_indices, path_indices[1:]):
            edges.append(self.edges[index][next_index])
        return edges

    def convert_path_to_edges(self, path):
        path_indices = [self.nodes.index(obj) for obj in path]
        return self.convert_path_indices_to_edges(path_indices)


class LayoutManager(object):
    def __init__(self, reasoning):
        self.reasoning = reasoning
        self.lines = []
        self.segments_graph = Graph()

    def add_segment(self, segment):
        # print 'add_segment()'
        if segment not in self.reasoning.objects:
            self.reasoning.objects.add_object(segment)

        for end in segment.ends:
            if not self.segments_graph.has_node(end):
                self.segments_graph.add_node(end)

        if not self.segments_graph.has_edge(*segment.ends):
            self.segments_graph.create_edge(*segment.ends)
            self.handle_new_segment(segment)

    def handle_point_on_segment(self, relation):
        if not isinstance(relation, statements.PointOnSegment):
            return

        point, segment = relation.args
        line = self.get_or_create_line(*segment.ends)

        for line_point in line.points:
            self.add_segment(
                objects.Segment(line_point, point)
            )

    def handle_new_segment(self, segment):
        # print 'handle_new_segment({})'.format(segment)
        self.add_segment(segment)
        # print 'edges', self.segments_graph.edges

        for node in self.segments_graph.nodes:
            if self.segments_graph.has_edges(node, segment.ends):
                triangle = objects.Triangle(node, segment.end_1, segment.end_2)
                self.reasoning.objects.add_object_and_dependencies(triangle)

    def get_or_create_line(self, point_1, point_2):
        for line in self.lines:
            if point_1 in line.points and point_2 in line.points:
                return line
        line = objects.Line(point_1, point_2)
        self.lines.append(line)
        return line

    def line_exists(self, *points):
        for line in self.lines:
            if all(point in line.points for point in points):
                continue
            return True
        return False


class EqualityManager(Justifier):
    """
    Stores only objects participated in equality relations
    """
    relation_classes = {
        float: statements.FloatEqualityRelation,
        objects.Point: statements.PointsEqualityRelation,
        objects.Segment: statements.SegmentsEqualityRelation,
        objects.Angle: statements.AnglesEqualityRelation,
        objects.Triangle: statements.TrianglesEqualityRelation,
    }
    atomic_types = (objects.Point, objects.MeasureResult, )

    def __init__(self, reasoning):
        super(EqualityManager, self).__init__(reasoning)
        self.registry = defaultdict(lambda: EqualityGraph())

    def find_relation_class(self, obj):
        return self.relation_classes.get(get_effective_type(obj))

    def already_added(self, obj):
        effective_type = get_effective_type(obj)

        res = self.registry[effective_type].has_node(obj)
        # if not res:
        #     print 'check already added {}, res={}'.format(obj, res)
        return res

    def add(self, obj):
        if not self.already_added(obj):
            self.registry[get_effective_type(obj)].add_node(obj)

    def equal(self, args, node_index):
        # print 'call equal for {} and {}, types {} and {}'.format(args[0], args[1], type(args[0]), type(args[1]))
        arg_type = get_effective_type(args[0])
        self.registry[arg_type].create_equality_edge(args[0], args[1], node_index)

        if arg_type == objects.Point:
            ### NEED TRANSFORMATION NODE
            for point in args:
                for arg_type, values in self.reasoning.objects.find_by_point(point).iteritems():
                    for value in values:
                        simplified_value = self.simplify_object(value)
                        if value != simplified_value:
                            self.equal([value, simplified_value], node_index)

    def iter_tuples_of_type(self, arg_types):
        all_args = []

        for arg_type in arg_types:
            args_of_type = []
            graph = self.registry[arg_type]

            if arg_type in self.atomic_types:
                components = graph.split_to_connected_components()
                for component in components:
                    args_of_type.append(self.find_simplest_of_group(component))

            elif issubclass(arg_type, objects.CompositeObject):
                for obj in graph.nodes:
                    if isinstance(obj, CompositeObject):
                        simplified_obj = self.simplify_object(obj)
                        if simplified_obj not in args_of_type:
                            # print 'obj {} was simplified to {}'.format(obj, simplified_obj)
                            args_of_type.append(simplified_obj)
                    else:
                        # FunctionCall maybe
                        args_of_type.append(obj)
            else:
                args_of_type = graph.nodes

            if args_of_type:
                all_args.append(args_of_type)
            else:
                raise NoObjectsOfType('No objects of type {}'.format(arg_type.type_verbose))

        # print 'tuples of type', Logger.to_unicode(all_args)
        return itertools.product(*all_args)

    def find_simplest_of_group(self, group, compare_with=None):
        res = sorted(group, key=lambda e: e.complexity)[0]
        if compare_with is None:
            return res
        else:
            if res.complexity == compare_with.complexity:
                return compare_with
            else:
                return res

    def simplify_object(self, arg):
        if isinstance(arg, objects.CompositeObject):
            if arg.complexity == arg.min_complexity:
                return arg
            simplified_atoms = map(self.simplify_object, arg.atoms)
            return arg.__class__(*simplified_atoms)
        elif isinstance(arg, objects.FunctionCall):
            try:
                arg_eval = arg.eval()
            except NotImplementedError:
                arg_type = get_effective_type(arg)
                group = self.registry[arg_type].collect_connected_nodes(arg)
                return self.find_simplest_of_group(group, arg)
            else:
                relation_class = self.find_relation_class(arg)
                relation = relation_class(arg, arg_eval)
                node_index = self.reasoning.relations.append_transformation_node(relation)
                self.equal([arg, arg_eval], node_index)
                return arg_eval
        elif isinstance(arg, ConstAlgebraicExpression):
            return arg
        elif isinstance(arg, AlgebraicExpression):
            if arg.args:
                simplified_args = map(self.simplify_object, arg.args)
                return arg.__class__(operator=arg.operator, args=simplified_args, name=arg.name)
            else:
                return arg
        elif isinstance(arg, self.atomic_types):
            arg_type = get_effective_type(arg)
            group = self.registry[arg_type].collect_connected_nodes(arg)
            return self.find_simplest_of_group(group, arg)
        else:
            return arg

    def simplify_relation(self, relation):
        simplified_args = map(self.simplify_object, relation.args)
        simplified = simplified_args != relation.args
        new_relation = relation.__class__(*simplified_args)

        if new_relation.is_tautology():
            simplified = False

        return simplified, new_relation

    def simplify_statement(self, statement):
        # print 'simplify_statement {}'.format(statement)
        if isinstance(statement, Relation):
            return self.simplify_relation(statement)
        elif isinstance(statement, LogicStatement):
            simplified_items = map(self.simplify_statement, statement.logic_items)
            simplified = all(item[0] for item in simplified_items)

            if statement.logic_op in ('and', 'or', 'not'):
                return simplified, LogicStatement(statement.logic_op, *[item[1] for item in simplified_items])
            elif statement.logic_op in ('exists', 'for_each'):
                return simplified, LogicStatement(statement.logic_op, statement.quantor_args, *simplified_items)
            else:
                return False, statement
        else:
            return False, statement

    def get_justifications(self, relation):
        # print 'EqualityManager.get_justifications for {}'.format(relation)
        if not isinstance(relation, statements.EqualityRelation):
            raise CanNotJustify

        justifications = []
        arg_type = get_effective_type(relation.args[0])
        graph = self.registry[arg_type]

        path = graph.find_path(*relation.args)
        if not path:
            raise CanNotJustify
        edges = graph.convert_path_to_edges(path)

        for edge in edges:
            node = self.reasoning.relations.nodes[edge.reasoning_node_index]
            justifications.append(
                Justification(node.relation, Justification.FOUND_IN_RELATIONS, node.index)
            )
        return justifications

    def __unicode__(self):
        return Logger.to_unicode(self.registry)


class Justification(object):
    IS_TAUTOLOGY = 1
    FOUND_IN_RELATIONS = 2
    UNDERLYING_STATEMENT_IS_FALSE = 4
    INTERNAL_CHECK = 5

    def __init__(self, statement, code=None, parent_index=None):
        self.statement = statement
        self.code = code or self.FOUND_IN_RELATIONS
        self.parent_index = parent_index

    def __unicode__(self):
        if self.code == self.IS_TAUTOLOGY:
            return u'{} is tautology'.format(self.statement)
        elif self.code == self.FOUND_IN_RELATIONS:
            return u'{} was found in relations'.format(self.statement)
        elif self.code == self.UNDERLYING_STATEMENT_IS_FALSE:
            return u'({}) is True because underlying statement is False'.format(self.statement)
        elif self.code == self.INTERNAL_CHECK:
            return u'{} is True due to internal check'.format(self.statement)


class Hypothesis(object):
    def __init__(self, theorem, consequence_getter, args, to_derive_item, missing_statement):
        self.theorem = theorem
        self.consequence_getter = consequence_getter
        self.args = args
        self.to_derive_item = to_derive_item
        self.missing_statement = missing_statement

    def __unicode__(self):
        return unicode(self.missing_statement)


class RelationsManager(object):
    """
    This class is supposed to store already proven statements
    """
    def __init__(self, reasoning, statements=None):
        self.nodes = []
        self.reasoning = reasoning
        self.algebraic_equality_nodes = []

        statements = statements or []

        for statement in statements:
            self.append_data_node(statement, initial=True)

    @property
    def data_nodes(self):
        return (node for node in self.nodes if node.type == ReasoningNode.Types.DATA)

    @property
    def final_nodes(self):
        return (node for node in self.nodes if node.type == ReasoningNode.Types.FINAL)

    def append_data_node(self, statement, parent_index=None, initial=False):
        items = self.split_to_atomic(statement)
        added_node_indices = []

        for item in items:
            # print 'append_data_node for {}'.format(item)
            if self.is_empty(item) or item in (node.relation for node in self.data_nodes):
                continue

            self.reasoning.objects.add_from_statement(item)

            if not initial:
                assert parent_index is not None

            parent_indices = [parent_index] if parent_index else None
            node = ReasoningNode(relation=item, parent_indices=parent_indices, index=len(self), initial=initial)
            self.nodes.append(node)
            added_node_indices.append(node.index)

            if parent_index is not None:
                parent = self.nodes[parent_index]
                parent.child_indices.append(node.index)

            if isinstance(item, statements.EqualityRelation):
                self.reasoning.equality_manager.equal(item.args, node.index)

            if isinstance(item, AlgebraicEqualityRelation):
                # print 'append algebraic equality relation {}'.format(item)
                self.algebraic_equality_nodes.append(node)

        return added_node_indices

    def append_logic_node(self, theorem, args, justifications, transformation_node_indices):
        parent_indices = [j.parent_index for j in justifications if j.parent_index]
        parent_indices += transformation_node_indices

        node = ReasoningNode(theorem=theorem, args=args, node_type=ReasoningNode.Types.LOGIC,
                             justifications=justifications,
                             parent_indices=parent_indices, index=len(self))
        self.nodes.append(node)
        return node.index

    def append_transformation_node(self, relation, justifications=None, parent_indices=None):
        # print 'append_transformation_node for {}'.format(relation)
        justifications = justifications or []
        if parent_indices is None:
            parent_indices = [j.parent_index for j in justifications]

        node = ReasoningNode(relation=relation, node_type=ReasoningNode.Types.TRANSFORMATION,
                             justifications=justifications,
                             parent_indices=parent_indices, index=len(self))
        self.nodes.append(node)
        return node.index

    def append_equation_node(self, transformations, parent_index):
        node = ReasoningNode(transformations=transformations, parent_indices=[parent_index],
                             node_type=ReasoningNode.Types.EQUATION, index=len(self))
        self.nodes.append(node)
        return node.index

    def append_final_node(self, statement, justifications=None, parent_index=None):
        if justifications:
            parent_indices = [j.parent_index for j in justifications]
        else:
            parent_indices = [parent_index]

        node = ReasoningNode(relation=statement, node_type=ReasoningNode.Types.FINAL,
                             justifications=justifications, parent_indices=parent_indices,
                             index=len(self))
        self.nodes.append(node)

    @staticmethod
    def is_empty(statement):
        if isinstance(statement, LogicStatement) and not statement.logic_items:
            return True
        else:
            return False

    def split_to_atomic(self, statement):
        if isinstance(statement, Relation):
            return [statement]
        elif isinstance(statement, LogicStatement) and statement.logic_op == 'and':
            return sum(map(self.split_to_atomic, statement.logic_items), [])
        elif isinstance(statement, LogicStatement) and statement.logic_op == 'exists':
            return self.split_to_atomic(statement.logic_items[0])
        elif isinstance(statement, statements.ExistsFactory):
            quantor_args = self.reasoning.objects.pick_unique_names(statement.sample_args)
            return [statement] + self.split_to_atomic(statement.apply(*quantor_args))
        else:
            return [statement]

    def index(self, relation):
        for node_index, node in enumerate(self.nodes):
            if node.type not in (ReasoningNode.Types.DATA, ReasoningNode.Types.TRANSFORMATION, ):
                continue
            if node.relation == relation:
                return node_index
        raise ValueError('{} not in nodes'.format(relation))

    def __len__(self):
        return len(self.nodes)

    def __contains__(self, item):
        return item in (node.relation for node in self.nodes)

    def build_node_usage_mask(self, node, usage_mask=None):
        if usage_mask is None:
            usage_mask = [0] * len(self)

        usage_mask[node.index] = 1

        # print 'node.relation {}'.format(node.relation)
        # print 'node.parent_indices {}'.format(node.parent_indices)
        # print 'node.type {}'.format(node.type)
        for parent_index in node.parent_indices:
            self.build_node_usage_mask(self.nodes[parent_index], usage_mask)

        return usage_mask


class TrivialJustifier(Justifier):
    def get_justifications(self, relation):
        if relation.is_tautology():
            justification = Justification(relation, code=Justification.IS_TAUTOLOGY)
            return [justification]
        elif relation in self.reasoning.relations:
            parent_index = self.reasoning.relations.index(relation)
            justification = Justification(relation, parent_index=parent_index)
            return [justification]
        else:
            raise CanNotJustify


class AnglesManager(Justifier):
    def __init__(self, reasoning):
        super(AnglesManager, self).__init__(reasoning)

    def get_justifications(self, relation):
        try:
            angle_1, angle_2 = relation.args
        except ValueError:
            raise CanNotJustify

        if not check_args_are_of_type(relation.args, (objects.Angle, objects.Angle)):
            raise CanNotJustify

        return self.reasoning.equality_manager.get_justifications(
            statements.AlgebraicEqualityRelation(
                FunctionCall(statements.AngleMeasure(), angle_1),
                FunctionCall(statements.AngleMeasure(), angle_2)
            )
        )


class TheoremManager(object):
    def __init__(self):
        self.theorems = []
        self.definitions = []

    def _append(self, theorem_class, theorem_kind):
        if theorem_kind == 'theorem':
            registry = self.theorems
        elif theorem_kind == 'definition':
            registry = self.definitions
        else:
            raise Exception('unknown theorem kind {}'.format(theorem_kind))

        registry.append(theorem_class)
        if theorem_class.is_reversible:
            registry.append(self.create_reverse_theorem(theorem_class))

    def add_theorems(self, theorems):
        for theorem_class in theorems:
            self._append(theorem_class, 'theorem')

    def add_definitions(self, definitions):
        for theorem_class in definitions:
            self._append(theorem_class, 'definition')

    def all(self):
        return self.theorems + self.definitions

    def create_reverse_theorem(self, theorem_class):
        # print 'create_reverse_theorem for {}'.format(theorem_class.verbose_name)
        class ReverseTheorem(theorem_class):
            verbose_name = 'Reverse {}'.format(theorem_class.verbose_name)

            @staticmethod
            def apply(*args):
                res = theorem_class.apply(*args)
                res.logic_items = res.logic_items[::-1]
                return res

        return ReverseTheorem


class EqualityEdge(GraphEdge):
    def __init__(self, start_index, end_index):
        super(EqualityEdge, self).__init__(start_index, end_index)
        self.reasoning_node_index = None


class EqualityGraph(Graph):
    edge_class = EqualityEdge
    # def __init__(self):
    #     self.registry = []
    #
    # @property
    # def nodes(self):
    #     return [record[0] for record in self.registry]
    #
    # def add_node(self, obj):
    #     self.registry.append(
    #         (obj, [])
    #     )
    #
    # def has_node(self, obj):
    #     return obj in (record[0] for record in self.registry)
    #
    # def has_edge(self, obj_1, obj_2):
    #     if not self.has_node(obj_1):
    #         return False
    #
    #     for record in self.registry:
    #         node, edges = record
    #         if node != obj_1:
    #             continue
    #         for edge in edges:
    #             if edge.args[1] == obj_2:
    #                 return True
    #         return False
    #
    # def find_record_by_node(self, obj):
    #     for record in self.registry:
    #         if record[0] == obj:
    #             return record

    def create_equality_edge(self, start_obj, end_obj, reasoning_node_index):
        edge = super(EqualityGraph, self).build_edge(start_obj, end_obj)
        edge.reasoning_node_index = reasoning_node_index
        self.add_edge(edge)


class ReasoningNode(object):
    class Types(object):
        LOGIC = 1
        DATA = 2
        FINAL = 3
        TRANSFORMATION = 4
        EQUATION = 5

    def __init__(self, relation=None, theorem=None, args=None, initial=False, node_type=None,
                 justifications=None, transformations=None, parent_indices=None, index=None):
        self.relation = relation
        self.theorem = theorem
        self.args = args
        self.initial = initial
        self.type = node_type or self.Types.DATA
        self.justifications = justifications
        self.transformations = transformations or []
        self.parent_indices = parent_indices or []
        self.child_indices = []
        self.index = index


class Reasoning(object):
    LOGIC_VALUE_UNKNOWN = None

    def __init__(self, relations=None, to_derive=None, to_calculate=None):
        self.objects = ObjectsManager(self)
        self.layout = LayoutManager(self)
        self.equality_manager = EqualityManager(self)

        self.logger = Logger()

        self.theorems = TheoremManager()
        self.theorems.add_theorems(statements.theorems)
        self.theorems.add_definitions(statements.definitions)

        self.to_derive = to_derive or []
        self.to_calculate = to_calculate or []
        self.calculated_values = []

        self.relations = RelationsManager(self, relations)

        for statement in self.to_derive:
            self.objects.add_from_statement(statement)

        self.angles_manager = AnglesManager(self)

        self.justifiers = (TrivialJustifier(self), self.equality_manager, )

        self.children = []

        self.theorems_and_args_used = []

        self.hypothesis = None
        self.done = False
        self.depth = 0
        self.child_index = 0

        self.inspect_relations()

    def inspect_relations(self):
        for node in self.relations.data_nodes:
            if type(node.relation) == statements.EqualityRelation:
                raise Exception('Warning: {} is plain equality relation'.format(node.relation))

        for relation in self.to_derive:
            if type(relation) == statements.EqualityRelation:
                raise Exception('Warning: {} is plain equality relation'.format(relation))

    def get_calculated_value(self, variable):
        # variable is Variable or FunctionCall
        for current_variable, value in self.calculated_values:
            if current_variable == variable:
                return value

    def get_justifier(self, relation):
        for justifier in self.justifiers:
            if justifier.can_approve(relation):
                return justifier

    @classmethod
    def logic_and(cls, *args):
        for arg in args:
            if arg is True:
                continue
            elif arg is False:
                return False
            else:
                return cls.LOGIC_VALUE_UNKNOWN
        return True

    @classmethod
    def logic_or(cls, *args):
        for arg in args:
            if arg is True:
                return True
            elif arg is False:
                continue
            else:
                return cls.LOGIC_VALUE_UNKNOWN

        return False

    @classmethod
    def logic_not(cls, arg):
        if arg is True:
            return False
        elif arg is False:
            return True
        else:
            return cls.LOGIC_VALUE_UNKNOWN

    def get_logic_value(self, statement):
        # print 'get_logic_value({})'.format(statement)
        res = self.LOGIC_VALUE_UNKNOWN

        if isinstance(statement, LogicStatement):
            if statement.logic_op == 'and':
                res = self.logic_and(*map(self.get_logic_value, statement.logic_items))
            elif statement.logic_op == 'not':
                res = self.logic_not(self.get_logic_value(statement.logic_items[0]))
        elif isinstance(statement, Relation):
            justifier = self.get_justifier(statement)
            if justifier:
                res = True

        return res

    def is_true(self, relation):
        try:
            self.get_justifications(relation)
            return True
        except CanNotJustify:
            return False

    def get_justifications(self, statement):
        if isinstance(statement, LogicStatement):
            if statement.logic_op == 'and':
                return sum(
                    map(self.get_justifications, statement.logic_items), []
                )
            elif statement.logic_op == 'not':
                logic_value = self.is_true(statement.logic_items[0])
                if logic_value is True:
                    raise CanNotJustify
                elif logic_value is False:  # NEED REWORK !!!
                    raise CanNotJustify
                    # return [Justification(statement, Justification.UNDERLYING_STATEMENT_IS_FALSE)]
                else:
                    raise CanNotJustify
        elif isinstance(statement, Relation):
            justifier = self.get_justifier(statement)
            if justifier:
                return justifier.get_justifications(statement)
            else:
                # print 'no justifier for {}'.format(statement)
                raise CanNotJustify
        else:
            raise CanNotJustify

    def apply_theorem(self, theorem, consequence_getter, args):
        applied_theorem = theorem.apply(*args)

        print '{} was applied to args {}.'.format(theorem.verbose_name, self.logger.list_to_unicode(args))
        statement_from = applied_theorem.logic_items[0]

        atoms = self.relations.split_to_atomic(statement_from)
        justifications = []
        transformation_node_indices = []

        for atom in atoms:
            justifier = self.get_justifier(atom)
            atom_justifications = justifier.get_justifications(atom)
            if isinstance(justifier, TrivialJustifier):
                justifications += atom_justifications
            elif isinstance(justifier, EqualityManager):
                transformation_node_index = self.relations.append_transformation_node(
                    atom, justifications=atom_justifications)
                transformation_node_indices.append(transformation_node_index)

        logic_node_index = self.relations.append_logic_node(theorem, args, justifications, transformation_node_indices)

        derived_item = consequence_getter(applied_theorem)
        # print 'result before simplify is {}'.format(self.logger.to_unicode(derived_item))
        data_node_indices = self.relations.append_data_node(derived_item, parent_index=logic_node_index)

        for data_node_index in data_node_indices:
            relation = self.relations.nodes[data_node_index].relation
            status, relation = self.equality_manager.simplify_statement(relation)
            if status:
                transformation_node_index = self.relations.append_transformation_node(
                    relation, parent_indices=[data_node_index])
                self.relations.append_data_node(relation, transformation_node_index)

    def apply_hypothesis(self, hypothesis):
        self.apply_theorem(hypothesis.theorem, hypothesis.consequence_getter, hypothesis.args)

    def find_aliases_by_condition(self, theorem, args):
        # if isinstance(theorem, statements.SideAngleSideCongruenceTheorem):
        #     print 'find aliases for theorem {}, args {}'.format(theorem.verbose_name, args)
        for aliases_tuple in statements.iter_aliases_tuples(*args):
            statement_from = theorem.apply(*aliases_tuple).logic_items[0]
            if self.is_true(statement_from):
                return aliases_tuple
        raise NoSatisfyingAliases

    def find_all_args_by_type_and_layout(self, theorem):
        args_alternatives = []

        # print 'find_all_args_by_type_and_layout({})'.format(theorem.verbose_name)

        # print 'nodes', Logger.to_unicode(self.equality_manager.registry[objects.Triangle].nodes)
        for args in self.equality_manager.iter_tuples_of_type(theorem.arg_types):
            # print u'checking layout for {}'.format(self.logger.list_to_unicode(args))
            try:
                layout = theorem.get_args_layout(*args)
            except (statements.ValidationError, AssertionError, ):
                continue

            if self.is_true(layout):
                args_alternatives.append(args)

        # print 'args_alternatives {}'.format(Logger.to_unicode(args_alternatives))
        return args_alternatives

    def find_all_aliases_by_condition_and_layout(self, theorem, *args):
        condition_is_args_exists_factory = hasattr(theorem, 'condition_class')
        factories = [node.relation for node in self.relations.data_nodes if isinstance(node.relation, ExistsFactory)]

        # if condition_is_args_exists_factory:
        #     print 'check condition class for {}'.format(theorem.verbose_name)

        for aliases_tuple in statements.iter_aliases_tuples(*args):
            if condition_is_args_exists_factory:
                for factory in factories:
                    if theorem.condition_class != factory.consequence_factory_type:
                        continue

                    condition_args = theorem.build_condition_args(*aliases_tuple)
                    consequence_factory_args = factory.build_consequence_factory_args(*aliases_tuple)

                    if condition_args == consequence_factory_args:
                        print 'yield {}'.format(self.logger.list_to_unicode(aliases_tuple))
                        yield aliases_tuple

            applied = theorem.apply(*aliases_tuple)
            layout = theorem.get_aliases_layout(*aliases_tuple)
            # print 'check condition', unicode(applied.logic_items[0]), self.get_logic_value(applied.logic_items[0])
            if self.is_true(applied.logic_items[0]) and self.is_true(layout):
                yield aliases_tuple

    def find_aliases_by_consequence(self, theorem, consequence_getter, args, consequence):
        for aliases_tuple in statements.iter_aliases_tuples(*args):
            # print 'try aliases {}'.format(aliases_tuple)
            applied = theorem.apply(*aliases_tuple)

            if consequence_getter(applied) == consequence:
                return aliases_tuple
        raise NoSatisfyingAliases

    def find_all_aliases_by_consequence(self, theorem, consequence_getter, args, consequence):
        res = []

        for aliases_tuple in statements.iter_aliases_tuples(*args):
            # print u'try aliases {}'.format(self.logger.list_to_unicode(aliases_tuple))
            applied = theorem.apply(*aliases_tuple)
            real_consequence = consequence_getter(applied)

            if isinstance(consequence, ExistsFactory) and isinstance(real_consequence, ExistsFactory):
                if real_consequence.is_partial_of(consequence):
                    # print u'{} is partial of {}'.format(real_consequence, consequence)
                    res.append(aliases_tuple)
            elif real_consequence == consequence:
                res.append(aliases_tuple)

        return res

    def find_theorems_by_quantor_arg_types(self, *quantor_arg_types):
        print 'find_theorems_by_quantor_arg_types({})'.format(map(unicode, quantor_arg_types))
        res = []

        for theorem in statements.theorems + statements.definitions:
            sampled_theorem = theorem.apply(*theorem.sample_args)
            statement_to = sampled_theorem.logic_items[1]

            if isinstance(statement_to, ExistsFactory):
                if any(arg_type in statement_to.arg_types for arg_type in quantor_arg_types):
                    res.append(
                        (theorem, lambda x: x.logic_items[1])
                    )
            elif isinstance(statement_to, LogicStatement) and statement_to.logic_op == 'and':
                for index, consequence in enumerate(statement_to.logic_items):
                    if isinstance(consequence, ExistsFactory):
                        # print 'statement_to', unicode(statement_to)
                        # print 'existsfactory consequence', unicode(consequence)
                        # print 'consequence.arg_types', self.logger.list_to_unicode(consequence.arg_types)
                        if any(arg_type in consequence.arg_types for arg_type in quantor_arg_types):
                            res.append(
                                (theorem, lambda x: x.logic_items[1].logic_items[index])
                            )

        print u'theorems by quantor args: {}'.format(self.logger.list_to_unicode([t[0].verbose_name for t in res]))
        return res

    @staticmethod
    def find_definition(relation):
        for definition in statements.definitions:
            if isinstance(relation, definition.subject):
                return definition

    def filter_missing_arg_types(self, *arg_types):
        res = []

        for arg_type in arg_types:
            if arg_type not in self.objects.registry:
                res.append(arg_type)

        return res

    def mark_theorem_as_used(self, theorem, args):
        self.theorems_and_args_used.append(
            theorem.get_cmp_key(args)
        )

    def was_theorem_with_args_used(self, theorem, args):
        cmp_key = theorem.get_cmp_key(args)
        return cmp_key in self.theorems_and_args_used

    def theorem_gives_tautology(self, theorem, args):
        cmp_key = theorem.get_cmp_key(args)
        if isinstance(cmp_key, Relation) and cmp_key.is_tautology():
            return True

        consequence = theorem.apply(*args).logic_items[1]
        if isinstance(consequence, Relation) and consequence.is_tautology():
            return True

    def direct_derive(self):
        # Suppose that to_derive_item is Relation instance or ExistsFactory instance
        loop_index = -1

        while True:
            loop_index += 1
            print 'direct derive, iteration={}'.format(loop_index)

            for algebraic_equality_node in self.relations.algebraic_equality_nodes:
                equation = algebraic_equality_node.relation
                variables = self.find_relation_variables(equation)
                if len(variables) == 1:
                    variable = variables[0]
                    print 'found equation {} on variable {}'.format(ExpressionParser().to_unicode(equation), variable)

                    if variable in self.to_calculate:
                        print 'solving...'
                        solver = LinearEquationSolver(equation)
                        value = solver.solve(variable)
                        print 'value = {}'.format(value)
                        self.to_calculate.pop(self.to_calculate.index(variable))
                        self.calculated_values.append(
                            (variable, value)
                        )
                        equation_node_index = self.relations.append_equation_node(
                            solver.history, algebraic_equality_node.index)
                        self.relations.append_final_node(
                            AlgebraicEqualityRelation(variable, value), parent_index=equation_node_index)
            self.relations.algebraic_equality_nodes = []

            prev_relations_count = len(self.relations)

            rest_to_derive = []

            for to_derive_item in self.to_derive:
                justifier = self.get_justifier(to_derive_item)
                if not justifier:
                    rest_to_derive.append(to_derive_item)
                    continue

                if isinstance(justifier, TrivialJustifier):
                    justifications = justifier.get_justifications(to_derive_item)
                    self.relations.append_final_node(to_derive_item, justifications)
                elif isinstance(justifier, EqualityManager):
                    transformation_node_index = self.relations.append_transformation_node(
                        to_derive_item, justifier.get_justifications(to_derive_item)
                    )

                    self.relations.append_final_node(to_derive_item, parent_index=transformation_node_index)

            self.to_derive = rest_to_derive

            if not self.to_derive and not self.to_calculate:
                self.done = True
                break

            for theorem in self.theorems.all():
                # print 'try theorem {}'.format(theorem.verbose_name)
                try:
                    args_by_type_and_layout = self.find_all_args_by_type_and_layout(theorem)
                except NoObjectsOfType as e:
                    # print e.message
                    continue

                for args in args_by_type_and_layout:
                    # if not theorem.args_are_reasonable(*args):
                    #     continue
                    if self.was_theorem_with_args_used(theorem, args):
                        continue

                    # print 'args {} for theorem {}'.format(self.logger.list_to_unicode(args), theorem.verbose_name)
                    for aliases in self.find_all_aliases_by_condition_and_layout(theorem, *args):
                        # print 'aliases {}'.format(Logger.to_unicode(aliases))
                        if self.theorem_gives_tautology(theorem, aliases):
                            continue
                        self.mark_theorem_as_used(theorem, aliases)
                        print 'aliases {} for theorem {}'.format(
                            self.logger.list_to_unicode(aliases), theorem.verbose_name)
                        self.apply_theorem(theorem, lambda x: x.logic_items[1], aliases)
                        break

            if len(self.relations) == prev_relations_count:
                break

        if self.done:
            usage_masks = []
            for final_node in self.relations.final_nodes:
                usage_masks.append(self.relations.build_node_usage_mask(final_node))

            usage_mask = map(int, [any(usages) for usages in zip(*usage_masks)])
            for is_used, node in zip(usage_mask, self.relations.nodes):
                if not is_used:
                    continue
                if node.initial:
                    continue
                if node.type == ReasoningNode.Types.DATA:
                    continue

                children = []
                for child_index in node.child_indices:
                    if usage_mask[child_index]:
                        child = self.relations.nodes[child_index]
                        children.append(child)

                if len(node.parent_indices) == 1:
                    parent = self.relations.nodes[node.parent_indices[0]]
                else:
                    parent = None

                self.logger.append(self.logger.render_node(node, children, parent))

        return self.done

    def find_relation_variables(self, relation):
        if not isinstance(relation, AlgebraicEqualityRelation):
            return False

        left_variables = self.find_expression_variables(relation.left_part)
        right_variables = self.find_expression_variables(relation.right_part)

        variables = self.make_unique(left_variables + right_variables)
        return variables

    def find_expression_variables(self, expression):
        if isinstance(expression, (Variable, FunctionCall)):
            return [expression]
        elif isinstance(expression, ConstAlgebraicExpression):
            return []

        variables = []
        for arg in expression.args:
            if isinstance(arg, (Variable, FunctionCall)):
                variables.append(arg)
            elif isinstance(arg, AlgebraicExpression):
                variables.extend(self.find_expression_variables(arg))
            else:
                print 'unknown arg {}'.format(arg)
        return self.make_unique(variables)

    @staticmethod
    def make_unique(sequence):
        res = []
        for item in sequence:
            if item not in res:
                res.append(item)
        return res

    def create_child(self, node_type=None, to_derive=None):
        child = self.clone()
        child.depth += 1
        child.child_index = len(self.children)

        child.logger.clear()
        child.node_type = node_type or 'single'

        if to_derive is not None:
            child.to_derive = to_derive

        child.parent = self
        child.children = []

        return child

    def create_child_from_hypothesis(self, hypothesis):
        missing_statement = hypothesis.missing_statement

        if isinstance(missing_statement, Relation):
            child = self.create_child('single', [missing_statement])
        else:
            child = self.create_child('and', missing_statement.logic_items)

        child.logger.append(
            u'appropriate theorem for {} is {}'.format(
                hypothesis.to_derive_item, hypothesis.theorem
            )
        )
        child.logger.append(u'with args {}'.format(map(unicode, hypothesis.args)))

        child.hypothesis = hypothesis

        return child

    def find_successful_child(self):
        for child in self.children:
            if child.done:
                return child

    def split(self):
        children = []

        for item in self.to_derive:
            child = self.create_child('single', [item])
            children.append(child)

        self.children = children

    def clone(self):
        return deepcopy(self)

    def go(self):
        self.logger.append(
            self.logger.render(
                'initial.html',
                relations=[node.relation for node in self.relations.nodes],
                to_derive=self.to_derive,
                to_calculate=self.to_calculate
            )
        )

        try:
            self.direct_derive()
        finally:
            self.logger.flush()
