# coding: utf-8

from __future__ import unicode_literals

import itertools
from itertools import permutations
from decimal import Decimal


class SpecialChars(object):
    ANGLE = '∠'
    TRIANGLE = '△'


def short_unicode(obj):
    if hasattr(obj, '__short_unicode__'):
        return obj.__short_unicode__()
    else:
        return unicode(obj)


class ValidationError(Exception):
    pass


def sets_equal(set_1, set_2):
    if len(set_1) != len(set_2):
        return False

    set_2_as_list = list(set_2)

    for element in set_1:
        try:
            set_2_as_list.pop(set_2_as_list.index(element))
        except ValueError:
            return False

    return True


class MathObject(object):
    type_verbose = None

    def __init__(self, obj_name=None):
        if not hasattr(self, 'name'):
            self.name = obj_name

    def __repr__(self):
        return unicode(self)

    def __unicode__(self):
        return u'{} {}'.format(self.type_verbose, self.name)

    def __eq__(self, other):
        if type(self) != type(other):
            return False

        return unicode(self.name) == unicode(other.name)

    def __ne__(self, other):
        return not self == other

    @property
    def dependencies(self):
        return tuple()

    @property
    def complexity(self):
        return 1


class ComplexName(object):
    def __init__(self, basic_name, index=None):
        self.basic_name = basic_name
        self.index = index

    def next(self):
        if self.index is None:
            return ComplexName(self.basic_name, 1)
        else:
            return ComplexName(self.basic_name, self.index + 1)

    def __unicode__(self):
        if self.index is None:
            return self.basic_name
        else:
            return '{}{}'.format(self.basic_name, self.index)


class Set(MathObject):
    type_verbose = u'set'
    element_type = None


class FiniteSet(Set):
    # Looks like python sets use 'is' operator in __eq__ and __contains__ methods.
    # That's why 'in' and '==' operators don't work for sets consisting of MathObject instances

    type_verbose = u'finite set'

    def __init__(self, *elements):
        super(FiniteSet, self).__init__('')

        self.elements = []

        for element in elements:
            self.add(element)

        if elements:
            self.element_type = type(elements[0])
        else:
            self.element_type = None

    def add(self, element):
        if element not in self.elements:
            self.elements.append(element)

    def __len__(self):
        return len(self.elements)

    def __unicode__(self):
        return u'{{{}}}'.format(', '.join(map(unicode, self.elements)))

    def __and__(self, other):
        if not isinstance(other, FiniteSet):
            raise TypeError('{} is not an instance of Set'.format(other))

        intersection = FiniteSet()
        for element in self.elements:
            if element in other:
                intersection.add(element)

        return intersection

    def __or__(self, other):
        if not isinstance(other, FiniteSet):
            raise TypeError('{} is not an instance of Set'.format(other))

        union = FiniteSet(*self.elements)

        for element in other.elements:
            union.add(element)

        return union

    def __contains__(self, item):
        for element in self.elements:
            if element == item:
                return True
        return False

    def contains_in(self, other):
        for element in self.elements:
            if element not in other:
                return False
        return True

    def __eq__(self, other):
        if not isinstance(other, FiniteSet):
            return False

        return self.contains_in(other) and other.contains_in(self)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __iter__(self):
        return iter(self.elements)


class OrderedFiniteSet(FiniteSet):
    type_verbose = u'ordered set'

    def __init__(self, *elements):
        super(OrderedFiniteSet, self).__init__(*elements)

    def add(self, element):
        self.elements.append(element)

    def __getitem__(self, item):
        return self.elements[item]


class Function(MathObject):
    _domain_element_type = None
    _codomain_element_type = None
    domain_type = None
    codomain_type = None
    domain = None
    codomain = None

    @property
    def domain_element_type(self):
        return self._domain_element_type or self.domain_type.element_type

    @property
    def codomain_element_type(self):
        return self._codomain_element_type or self.codomain_type.element_type

    @property
    def effective_name(self):
        return self.name or self.type_verbose

    def call_to_unicode(self, arg):
        return u'{}({})'.format(self.effective_name, arg)

    def __call__(self, arg):
        raise NotImplementedError

    def __unicode__(self):
        # print 'unicode()', self.type_verbose, type(self) #, self.domain, self.codomain
        if self.domain and self.codomain:
            return '{} {}: {} -> {}'.format(self.type_verbose, self.name, self.domain, self.codomain)
        elif self.name:
            return '{} {}'.format(self.type_verbose, self.name)
        else:
            return self.type_verbose

    def __eq__(self, other):
        # print '__eq__({}, {})'.format(self, other)
        # print 'len(self.domain)={}'.format(len(self.domain))
        if not isinstance(other, Function):
            return False
        if self.domain != other.domain:
            return False
        if self.domain is None:
            return False
        try:
            for element in self.domain:
                if self(element) != other(element):
                    return False
        except NotImplementedError:
            return False

        return True


class BijectionBase(Function):
    type_verbose = u'bijection'

    def __init__(self, domain, codomain=None, name=None):
        MathObject.__init__(self, name)

        self.domain = domain
        self.codomain = codomain or domain


class PointsBijectionManager(object):
    def __init__(self, domain):
        if isinstance(domain, (list, tuple)):
            domain = PointsFiniteSet(*domain)

        self.permutation_manager = PermutationManager(len(domain))

        self.domain = domain

    def id(self):
        return IdPointsBijection(self.domain, self.domain)

    def get_object_by_name(self, name):
        return PointsBijection(self.domain, self.domain, name)


class PermutationManager(object):
    def __init__(self, length):
        self.length = length
        self.codes = [
            dict(zip(range(self.length), p)) for p in itertools.permutations(range(self.length), self.length)
        ]
        self.id_code = {i: i for i in range(self.length)}

    def all(self):
        return [Permutation(code) for code in self.codes]

    def id(self):
        return Permutation(self.id_code)


class Permutation(Function):
    type_verbose = u'permutation'
    arg_types = (OrderedFiniteSet,)
    return_value_type = OrderedFiniteSet

    def __init__(self, code, name=''):
        super(Permutation, self).__init__(name)
        self.code = code
        self.length = len(code)

    @staticmethod
    def from_string(name):
        return Permutation([], name)

    class LengthMismatch(Exception):
        pass

    def apply(self, sequence):
        if len(sequence) != self.length:
            raise Permutation.LengthMismatch

        return [sequence[self.code[i]] for i in xrange(self.length)]

    def __call__(self, sequence):
        return self.apply(sequence)

    def as_coordinate(self, arg_type, sequence):
        class PermutationCoordinate(Function):
            type_verbose = u'permutation coordinate'
            arg_types = (arg_type, )
            return_value_type = arg_type

            def __init__(self, permutation):
                super(PermutationCoordinate, self).__init__(permutation.name)
                self.permutation = permutation

            def __call__(self, element):
                index = sequence.index(element)
                return self.permutation(sequence)[index]

        return PermutationCoordinate(self)


class Point(MathObject):
    type_verbose = u'point'

    def __init__(self, name):
        super(Point, self).__init__(name)

    def __unicode__(self):
        return self.name

    def __short_unicode__(self):
        return self.name


class PointsFiniteSet(FiniteSet):
    element_type = Point


class GenericPoint(object):
    def __init__(self, point):
        self.elements = [point]
        self.simplest_element = point

    def add(self, point):
        if point not in self.elements:
            self.elements.append(point)

            if point.complexity < self.simplest_element.complexity:
                self.simplest_element = point

    @property
    def name(self):
        return self.simplest_element.name


class PointsBijection(BijectionBase):
    domain_type = PointsFiniteSet
    codomain_type = PointsFiniteSet


class IdPointsBijection(PointsBijection):
    name = 'id'

    def __call__(self, arg):
        return arg


class CompositeObject(MathObject):
    @property
    def atoms(self):
        raise NotImplementedError

    @property
    def min_complexity(self):
        return len(self.atoms)

    def replace_atom(self, old_atom, new_atom):
        atoms = self.atoms[:]
        for index, atom in enumerate(atoms):
            if atom == old_atom:
                atoms[index] = new_atom
                break

        return self.__class__(*atoms)


class Segment(CompositeObject):
    type_verbose = u'segment'

    def __init__(self, end_1, end_2):
        super(Segment, self).__init__(u'{}{}'.format(end_1, end_2))
        self.end_1 = end_1
        self.end_2 = end_2

    @staticmethod
    def from_string(ab):
        return Segment(*map(Point, ab))

    @property
    def ends(self):
        return FiniteSet(self.end_1, self.end_2)

    @property
    def atoms(self):
        return self.ends

    @property
    def dependencies(self):
        return self.ends

    def __eq__(self, other):
        if not isinstance(other, Segment):
            return False

        return self.ends == other.ends

    def __unicode__(self):
        return self.name

    def __short_unicode(self):
        return u'segment {}{}'.format(self.end_1.name, self.end_2.name)

    @property
    def complexity(self):
        return self.end_1.complexity + self.end_2.complexity


class Line(MathObject):
    type_verbose = u'line'

    def __init__(self, *points):
        super(Line, self).__init__(u'{}{}'.format(points[0], points[1]))
        self.points = points or []
        self.order = []

    def add_point_to_segment(self, point, segment):
        end_1, end_2 = segment.ends

        order_value = self.get_order_value(end_1, end_2)
        if not order_value:
            # That's incorrect
            order_value = 1
            self.order.append(
                (end_1, end_2)
            )
        if order_value > 0:
            self.order.extend([
                (end_1, point),
                (point, end_2),
            ])
        else:
            self.order.extend([
                (end_2, point),
                (point, end_1),
            ])

    def get_order_value(self, point_1, point_2):
        for pair in self.order:
            if (point_1, point_2) == pair:
                return 1
            elif (point_2, point_1) == pair:
                return -1

    @property
    def dependencies(self):
        return self.points

    def __unicode__(self):
        return self.name


class Angle(CompositeObject):
    type_verbose = u'angle'

    def __init__(self, point_1, vertex, point_2):
        super(Angle, self).__init__(u'{}{}{}'.format(point_1, vertex, point_2))

        self.point_1 = point_1
        self.vertex = vertex
        self.point_2 = point_2

        self.side_1 = Segment(vertex, point_1)
        self.side_2 = Segment(vertex, point_2)

    @staticmethod
    def from_string(abc):
        return Angle(*map(Point, abc))

    @property
    def points(self):
        return FiniteSet(self.point_1, self.point_2)

    @property
    def points_and_vertex(self):
        return self.point_1, self.vertex, self.point_2

    @property
    def atoms(self):
        return self.points_and_vertex

    @property
    def dependencies(self):
        return self.points_and_vertex + (self.side_1, self.side_2, )

    def __eq__(self, other):
        if not isinstance(other, Angle):
            return False

        if self.vertex != other.vertex:
            return False

        return sets_equal(self.points, other.points)

    def divide_by_segment(self, segment):
        # print 'angle {}, segment {}'.format(self, segment)
        if segment.end_1 != self.vertex:
            assert segment.end_2 == self.vertex
            segment = Segment(segment.end_2, segment.end_1)

        return Angle(self.point_1, self.vertex, segment.end_2), Angle(self.point_2, self.vertex, segment.end_2)

    def __unicode__(self):
        return u'{}{}'.format(SpecialChars.ANGLE, self.name)

    @property
    def complexity(self):
        return sum(p.complexity for p in self.points_and_vertex)


class Triangle(CompositeObject):
    type_verbose = u'triangle'

    def __init__(self, point_a, point_b, point_c):
        super(Triangle, self).__init__(u'{}{}{}'.format(point_a, point_b, point_c))

        self.point_a = point_a
        self.point_b = point_b
        self.point_c = point_c

        self.side_a = Segment(point_b, point_c)
        self.side_b = Segment(point_c, point_a)
        self.side_c = Segment(point_a, point_b)

        self.angle_a = Angle(point_c, point_a, point_b)
        self.angle_b = Angle(point_a, point_b, point_c)
        self.angle_c = Angle(point_b, point_c, point_a)

    @staticmethod
    def from_string(abc):
        assert isinstance(abc, basestring) and len(abc) == 3

        return Triangle(*map(Point, abc))

    @property
    def points(self):
        return FiniteSet(self.point_a, self.point_b, self.point_c)

    @property
    def atoms(self):
        return self.points

    @property
    def sides(self):
        return FiniteSet(self.side_a, self.side_b, self.side_c)

    @property
    def angles(self):
        return FiniteSet(self.angle_a, self.angle_b, self.angle_c)

    def angle_at(self, point):
        expr = FunctionCall(AngleAt(self), point)
        try:
            return expr.eval()
        except NotImplementedError:
            return expr

    def side_opposite_to(self, point):
        expr = FunctionCall(SideOppositeTo(self), point)
        try:
            return expr.eval()
        except NotImplementedError:
            return expr

    @property
    def dependencies(self):
        return self.points | self.sides | self.angles

    def __eq__(self, other):
        if not isinstance(other, Triangle):
            return False

        return sets_equal(self.points, other.points)

    def __ne__(self, other):
        return not self.__eq__(other)

    @property
    def complexity(self):
        return sum(p.complexity for p in self.points)

    def __unicode__(self):
        return u'{}{}'.format(SpecialChars.TRIANGLE, self.name)


class ComplexTypeBase(object):
    types = tuple()

    def __unicode__(self):
        return 'ComplexType({})'.format(', '.join(map(unicode, self.types)))


def create_complex_type(*_types):
    class ComplexType(ComplexTypeBase):
        types = _types

    return ComplexType()


class ComplexArg(object):
    def __init__(self, *args):
        self.args = args
        self.type = create_complex_type(*map(get_effective_type, self.args))

    def __iter__(self):
        return iter(self.args)

    def __unicode__(self):
        return 'ComplexArg({})'.format(', '.join(map(unicode, self.args)))

    @property
    def complexity(self):
        return sum([arg.complexity for arg in self.args], 0)


class SideOppositeTo(Function):
    type_verbose = 'side opposite to'
    domain_type = None
    name = 'SideOppositeTo'

    _domain_element_type = Point
    _codomain_element_type = Segment

    def __init__(self, triangle):
        super(SideOppositeTo, self).__init__()
        self.triangle = triangle

    def __call__(self, arg):
        triangle, point = self.triangle, arg
        
        if point == triangle.point_a:
            return triangle.side_a
        elif point == triangle.point_b:
            return triangle.side_b
        elif point == triangle.point_c:
            return triangle.side_c
        else:
            raise NotImplementedError

    def call_to_unicode(self, arg):
        return '{}({}) in {}'.format(self.name, arg, self.triangle)

    def __unicode__(self):
        return '{} in {}'.format(self.name, self.triangle)


class AngleAt(Function):
    type_verbose = 'angle at'
    domain_type = None
    _domain_element_type = Point
    _codomain_element_type = Angle
    name = 'AngleAt'

    def __init__(self, triangle):
        super(AngleAt, self).__init__()
        self.triangle = triangle

    def __call__(self, arg):
        triangle, point = self.triangle, arg
        
        if point == triangle.point_a:
            return triangle.angle_a
        elif point == triangle.point_b:
            return triangle.angle_b
        elif point == triangle.point_c:
            return triangle.angle_c
        else:
            raise NotImplementedError

    def call_to_unicode(self, arg):
        return '{}({}) in {}'.format(self.name, arg, self.triangle)

    def __unicode__(self):
        return '{} in {}'.format(self.name, self.triangle)


class AlgebraicExpression(MathObject):
    arithmetic_ops = ('+', '*', '-', '/', )

    def __init__(self, operator=None, args=None, name=None):
        super(AlgebraicExpression, self).__init__(obj_name=name)

        self.operator = operator
        self.args = args

    @property
    def dependencies(self):
        return self.args

    def eval(self):
        return self.operator(*(arg.eval() for arg in self.args))

    @property
    def complexity(self):
        res = sum((arg.complexity for arg in self.args), 0)

        if self.operator in self.arithmetic_ops:
            operator_complexity = 1
        elif isinstance(self.operator, MathObject):
            operator_complexity = self.operator.complexity
        else:
            operator_complexity = 0

        res += operator_complexity

        return res

    def __unicode__(self):
        return ExpressionParser().to_unicode(self)

    def __eq__(self, other):
        if not isinstance(other, AlgebraicExpression):
            return False

        return self.operator == other.operator and self.args == other.args


def get_effective_type(arg):
    if isinstance(arg, FunctionCall):
        if arg.operator.codomain_element_type:
            res = arg.operator.codomain_element_type
        else:
            print 'unknown arg type for {}'.format(arg)
            return FunctionCall
    elif isinstance(arg, ComplexArg):
        res = arg.type
    else:
        res = type(arg)

    if issubclass(res, AlgebraicExpression):
        res = AlgebraicExpression
    elif issubclass(res, PointsBijection):
        res = PointsBijection

    return res


def get_effective_types(*args):
    return map(get_effective_type, args)


def check_args_are_of_type(args, expected_types):
    real_types = get_effective_types(*args)

    for arg, real_type, expected_type in zip(args, real_types, expected_types):
        if issubclass(expected_type, ComplexTypeBase):
            if issubclass(real_type, ComplexTypeBase):
                if real_type.types != expected_type.types:
                    return False
            else:
                return False
        else:
            if not issubclass(real_type, expected_type):
                return False

    return True


def check_arg_is_of_type(arg, expected_type):
    return check_args_are_of_type((arg, ), (expected_type, ))


class FunctionCall(AlgebraicExpression):
    type_verbose = 'function call'

    def __init__(self, function, arg):
        assert isinstance(function, Function)

        if not check_arg_is_of_type(arg, function.domain_element_type):
            print 'expected type {}'.format(function.domain_element_type)
            raise ValidationError('arg {} has wrong type'.format(arg))

        super(FunctionCall, self).__init__(function, [arg])

    def __eq__(self, other):
        if not isinstance(other, FunctionCall):
            return False

        return type(self.operator) == type(other.operator) and self.args == other.args

    def eval(self):
        eval_args = []

        for arg in self.args:
            if isinstance(arg, FunctionCall):
                try:
                    eval_args.append(arg.eval())
                except NotImplementedError:
                    eval_args.append(arg)
            else:
                eval_args.append(arg)

        return self.operator(*eval_args)

    @property
    def arg(self):
        return self.args[0]

    @property
    def complexity(self):
        return self.operator.complexity + self.arg.complexity

    def __unicode__(self):
        return self.operator.call_to_unicode(*self.args)


class ConstAlgebraicExpression(AlgebraicExpression):
    def __init__(self, value):
        super(ConstAlgebraicExpression, self).__init__(args=[value])

    @property
    def value(self):
        return self.args[0]

    def eval(self):
        return self.value

    @property
    def complexity(self):
        return 1

    def zero(self):
        return ConstAlgebraicExpression(0)

    def __eq__(self, other):
        if not isinstance(other, ConstAlgebraicExpression):
            return False

        return self.value == other.value

    def __unicode__(self):
        return unicode(self.value)

    def __add__(self, other):
        assert isinstance(other, ConstAlgebraicExpression)

        return ConstAlgebraicExpression(self.value + other.value)

    def __sub__(self, other):
        assert isinstance(other, ConstAlgebraicExpression)
        return ConstAlgebraicExpression(self.value - other.value)


class Variable(AlgebraicExpression):
    type_verbose = 'variable'

    def __init__(self, name):
        super(Variable, self).__init__(name=name)

    @property
    def complexity(self):
        return 1


class MeasureResult(ConstAlgebraicExpression):
    allowed_units = ('cm', 'mm', 'm', 'degree', )

    def __init__(self, value, unit=None):
        assert isinstance(value, (int, long, float, Decimal, ))
        #     value = ConstAlgebraicExpression(value)

        super(MeasureResult, self).__init__(value)
        assert unit in self.allowed_units

        self.unit = unit
        self.unit_plural = '{}s'.format(self.unit)

    @property
    def value(self):
        return self.args[0]

    @property
    def complexity(self):
        if isinstance(self.unit, MathObject):
            unit_complexity = self.unit.complexity
        else:
            unit_complexity = 1

        if isinstance(self.value, MathObject):
            value_complexity = self.value.complexity
        else:
            value_complexity = 1
        return value_complexity + unit_complexity

    def zero(self):
        return MeasureResult(0, self.unit)

    def __eq__(self, other):
        if not isinstance(other, MeasureResult):
            return False

        return self.value == other.value

    def __unicode__(self):
        return u'{} {}'.format(self.value, self.unit)

    def __add__(self, other):
        assert isinstance(other, MeasureResult)
        assert self.unit == other.unit

        return MeasureResult(self.value + other.value, self.unit)

    def __sub__(self, other):
        assert isinstance(other, MeasureResult)
        assert self.unit == other.unit

        return MeasureResult(self.value - other.value, self.unit)


class AngleMeasure(Function):
    name = 'angle_measure'
    _domain_element_type = Angle
    _codomain_element_type = MeasureResult

    # @classmethod
    # def call_to_unicode(cls, *args):
    #     angle = args[0]
    #     return unicode(angle)


class SegmentLength(Function):
    name = 'length'
    type_verbose = 'length'
    _domain_element_type = Segment
    _codomain_element_type = MeasureResult


class Plane(Set):
    element_type = Point

    def __unicode__(self):
        return u'Plane'


plane_instance = Plane()


class Transformation(Function):
    type_verbose = u'transformation'

    domain_type = Plane
    codomain_type = Plane
    domain = plane_instance
    codomain = plane_instance

    def __unicode__(self):
        # Skip T: Plane -> Plane form
        return MathObject.__unicode__(self)

    def __eq__(self, other):
        if not isinstance(other, Transformation):
            return False
        return unicode(self.name) == unicode(other.name)


class ExpressionParser(object):
    def need_braces(self, expression):
        if isinstance(expression, (Variable, FunctionCall, ConstAlgebraicExpression)):
            return False
        elif expression.operator == '*':
            return False
        else:
            return True

    def to_unicode(self, expression):
        if not isinstance(expression, AlgebraicExpression):
            return unicode(expression)
        elif isinstance(expression, Variable):
            return expression.name
        elif isinstance(expression, ConstAlgebraicExpression):
            return unicode(expression)
        elif isinstance(expression, FunctionCall):
            return '{}({})'.format(expression.operator.effective_name, self.to_unicode(expression.arg))
        elif expression.operator == '+':
            summands = []
            for index, arg in enumerate(expression.args):
                is_composite = self.need_braces(arg)
                if is_composite and index > 0:
                    summands.append('({})'.format(self.to_unicode(arg)))
                else:
                    summands.append('{}'.format(self.to_unicode(arg)))
            # print 'summands', summands
            return ' + '.join(summands)
        elif expression.operator == '-':
            if len(expression.args) == 1:
                arg = expression.args[0]
                if self.need_braces(arg):
                    return '-({})'.format(self.to_unicode(arg))
                else:
                    return '-{}'.format(self.to_unicode(arg))
            else:
                summands = []
                for index, arg in enumerate(expression.args):
                    is_composite = self.need_braces(arg)
                    if is_composite and index > 0:
                        summands.append('({})'.format(self.to_unicode(arg)))
                    else:
                        summands.append('{}'.format(self.to_unicode(arg)))

                return '{} - {}'.format(*summands[:2])
