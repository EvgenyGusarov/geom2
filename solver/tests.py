# coding: utf-8

from __future__ import unicode_literals

from copy import deepcopy
import unittest
from decimal import Decimal

import objects
from objects import Point, Segment, Angle, Triangle, FunctionCall, AngleMeasure, MeasureResult, FiniteSet, \
    AlgebraicExpression, ConstAlgebraicExpression, short_unicode, Transformation, Variable, SegmentLength, \
    ExpressionParser
import statements
from statements import iter_matches, LinearEquationSolver, AlgebraicEqualityRelation
import reasoning
from reasoning import Reasoning, ObjectsManager, Logger, Graph, GraphEdge, EqualityGraph, EqualityEdge


class FiniteSetTestCase(unittest.TestCase):
    def test_add(self):
        s = FiniteSet(1, 2)
        assert len(s) == 2
        s.add(1)
        assert len(s) == 2
        s.add(3)
        assert len(s) == 3

    def test_and(self):
        assert len(FiniteSet(1, 2) & FiniteSet(3, 4)) == 0
        assert len(FiniteSet(1, 2) & FiniteSet(3, 1)) == 1
        assert len(FiniteSet(1, 2) & FiniteSet(2, 1)) == 2

    def test_or(self):
        assert len(FiniteSet(1, 2) | FiniteSet(3, 4)) == 4
        assert len(FiniteSet(1, 2) | FiniteSet(3, 1)) == 3
        assert len(FiniteSet(1, 2) | FiniteSet(2, 1)) == 2

    def test_contains(self):
        assert 1 in FiniteSet(1, 2)
        assert 3 not in FiniteSet(1, 2)

    def test_contains_in(self):
        assert FiniteSet(1, 2).contains_in(FiniteSet(3, 2, 1))
        assert not FiniteSet(1, 2).contains_in(FiniteSet(3, 2))

    def test_eq(self):
        assert FiniteSet(1, 2) == FiniteSet(1, 2)
        assert FiniteSet(1, 2) == FiniteSet(2, 1)
        self.assertFalse(FiniteSet(1, 2) == FiniteSet(1, 2, 3))
        self.assertFalse(FiniteSet(1, 2) == FiniteSet(1, 3))

    def test_ne(self):
        self.assertFalse(FiniteSet(1, 2) != FiniteSet(1, 2))
        self.assertFalse(FiniteSet(1, 2) != FiniteSet(2, 1))
        self.assertTrue(FiniteSet(1, 2) != FiniteSet(1, 2, 3))
        self.assertTrue(FiniteSet(1, 2) != FiniteSet(1, 3))


class TriangleTestCase(unittest.TestCase):
    def test_points(self):
        triangle = Triangle.from_string('ABC')
        assert len(triangle.points) == 3
        assert Point('A') in triangle.points

    def test_atoms(self):
        triangle = Triangle.from_string('ABC')
        assert triangle.points == triangle.atoms

    def test_sides(self):
        triangle = Triangle.from_string('ABC')
        assert len(triangle.sides) == 3
        assert Segment.from_string('AB') in triangle.sides

    def test_angles(self):
        triangle = Triangle.from_string('ABC')
        assert len(triangle.angles) == 3
        assert Angle.from_string('ABC') in triangle.angles

    def test_angle_at(self):
        triangle = Triangle.from_string('ABC')
        angle = Angle.from_string('ABC')
        assert triangle.angle_at(Point('B')) == angle

    def test_side_opposite_to(self):
        triangle = Triangle.from_string('ABC')
        side = Segment.from_string('BC')
        assert triangle.side_opposite_to(Point('A')) == side

    def test_dependencies(self):
        triangle = Triangle.from_string('ABC')
        dependencies = triangle.dependencies
        assert len(dependencies) == 9
        assert triangle.points.contains_in(dependencies)
        assert triangle.sides.contains_in(dependencies)
        assert triangle.angles.contains_in(dependencies)

    def test_eq(self):
        assert Triangle.from_string('ABC') == Triangle.from_string('ABC')
        assert Triangle.from_string('ABC') == Triangle.from_string('CAB')

    def test_ne(self):
        assert Triangle.from_string('ABC') != Triangle.from_string('ABD')


class ReasoningLogicOpsTestCase(unittest.TestCase):
    def test_logic_ops(self):
        unknown = Reasoning.LOGIC_VALUE_UNKNOWN

        assert Reasoning.logic_and(True, False) is False
        assert Reasoning.logic_and(True, True) is True
        assert Reasoning.logic_and(True, unknown) is unknown

        assert Reasoning.logic_or(True, False) is True
        assert Reasoning.logic_or(False, False) is False
        assert Reasoning.logic_or(True, unknown) is True
        assert Reasoning.logic_or(unknown, unknown) is unknown

        assert Reasoning.logic_not(True) is False
        assert Reasoning.logic_not(False) is True
        assert Reasoning.logic_not(unknown) is unknown

        
class IterMatchesTestCase(unittest.TestCase):
    def test_iter_matches(self):
        matches = list(iter_matches((1, 2), (1, 2)))
        assert len(matches) == 2

        matches = list(iter_matches((1, 2, 3), (1, 2, 3)))
        assert len(matches) == 6


class LoggerTestCase(unittest.TestCase):
    def test_flatten_list(self):
        assert Logger.flatten_list(['a']) == ['a']
        assert Logger.flatten_list(['a', 'b']) == ['a', 'b']
        assert Logger.flatten_list(['a', ['b']]) == ['a', '', 'b']
        assert Logger.flatten_list(['a', ['b', 'c']]) == ['a', '', 'b', 'c']
        assert Logger.flatten_list([['b', 'c'], 'd']) == ['b', 'c', '', 'd']
        assert Logger.flatten_list(['a', ['b', 'c'], 'd']) == ['a', '', 'b', 'c', '', 'd']

        assert Logger.flatten_list([['a'], ['b'], ['c']]) == ['a', '', 'b', '', 'c']


class FunctionCallTestCase(unittest.TestCase):
    def test_triangle_from_function_calls(self):
        transformation = objects.Transformation('T')
        triangle = Triangle(
            FunctionCall(transformation, Point('A')),
            FunctionCall(transformation, Point('B')),
            FunctionCall(transformation, Point('C'))
        )
        assert unicode(triangle)


class ObjectsManagerTestCase(unittest.TestCase):
    def test_detect_triangles(self):
        r = Reasoning()

        for obj in (Segment.from_string('AB'), Segment.from_string('AC'), Segment.from_string('BC')):
            r.objects.add_object_and_dependencies(obj)

        assert Triangle.from_string('ABC') in r.objects


class BijectionTestCase(unittest.TestCase):
    def test_bijection_manager(self):
        t = Triangle.from_string('ABC')
        m = objects.PointsBijectionManager(t.points)
        id_function = m.id()
        assert id_function(Point('A')) == Point('A')


class ExistsFactoryTestCase(unittest.TestCase):
    def test_eq(self):
        pass


class EqualityRelationTestCase(unittest.TestCase):
    def test_tautology(self):
        segment_1 = Segment(Point('A'), Point('B'))
        segment_2 = Segment(Point('A'), Point('B'))
        segment_3 = Segment(Point('B'), Point('A'))
        segment_4 = Segment(Point('A'), Point('C'))

        assert statements.SegmentsEqualityRelation(segment_1, segment_2).is_tautology()
        assert statements.SegmentsEqualityRelation(segment_1, segment_3).is_tautology()
        assert not statements.SegmentsEqualityRelation(segment_1, segment_4).is_tautology()

    def test_segments_equality(self):
        statement_1 = statements.SegmentsEqualityRelation(
            Segment(Point('A'), Point('B')),
            Segment(Point('C'), Point('D'))
        )
        statement_2 = statements.SegmentsEqualityRelation(
            Segment(Point('C'), Point('D')),
            Segment(Point('A'), Point('B'))
        )
        statement_3 = statements.SegmentsEqualityRelation(
            Segment(Point('A'), Point('B')),
            Segment(Point('D'), Point('C'))
        )
        statement_4 = statements.SegmentsEqualityRelation(
            Segment(Point('A'), Point('B')),
            Segment(Point('D'), Point('E'))
        )

        assert statement_1 == statement_2
        assert statement_1 == statement_3
        assert statement_1 != statement_4

    def test_triangles_equality(self):
        statement_1 = statements.TrianglesEqualityRelation(
            Triangle.from_string('ABC'),
            Triangle.from_string('DEF')
        )
        statement_2 = statements.TrianglesEqualityRelation(
            Triangle.from_string('CBA'),
            Triangle.from_string('DEF')
        )
        statement_3 = statements.TrianglesEqualityRelation(
            Triangle.from_string('CAB'),
            Triangle.from_string('FDE')
        )
        assert statement_1 == statement_2
        assert statement_1 == statement_3

    def test_objects_equal(self):
        r = Reasoning()
        s = statements.PointsEqualityRelation(Point('A'), Point('B'))
        assert r.get_logic_value(s) is r.LOGIC_VALUE_UNKNOWN

        assert Point('A') == Point('A')
        assert Point('B') != Point('A')

        assert Angle.from_string('AOB') == Angle.from_string('BOA')
        assert Triangle.from_string('ABC') == Triangle.from_string('BCA')

    def test_deepcopy(self):
        x = Triangle.from_string('ABC')
        assert deepcopy(x) == Triangle.from_string('ABC')


class GraphTestCase(unittest.TestCase):
    def test_build_edge(self):
        graph = Graph()
        graph.add_node('a')
        graph.add_node('b')
        edge = graph.build_edge('a', 'b')
        assert isinstance(edge, GraphEdge)
        assert graph.get_edge_start(edge) == 'a'
        assert graph.get_edge_end(edge) == 'b'

    def test_create_edge(self):
        graph = Graph()
        graph.add_node('a')
        graph.add_node('b')
        graph.create_edge('a', 'b')
        assert graph.has_edge('a', 'b')
        assert graph.has_edge('b', 'a')

    def test_collect_connected_nodes(self):
        graph = Graph()
        graph.add_node('a')
        graph.add_node('b')
        graph.create_edge('a', 'b')
        graph.add_node('c')

        connected_nodes = graph.collect_connected_nodes('a')
        assert len(connected_nodes) == 2
        assert 'a' in connected_nodes
        assert 'b' in connected_nodes

        connected_nodes = graph.collect_connected_nodes('b')
        assert len(connected_nodes) == 2
        assert 'a' in connected_nodes
        assert 'b' in connected_nodes

        connected_nodes = graph.collect_connected_nodes('c')
        assert len(connected_nodes) == 1
        assert 'c' in connected_nodes

    def test_split_to_connected_components(self):
        graph = Graph()
        graph.add_node('a')
        graph.add_node('b')

        components = graph.split_to_connected_components()
        assert len(components) == 2

        graph.create_edge('a', 'b')

        components = graph.split_to_connected_components()
        assert len(components) == 1

        graph.add_node('c')

        components = graph.split_to_connected_components()
        assert len(components) == 2

    def test_find_path(self):
        graph = Graph()
        graph.add_node('a')
        graph.add_node('b')

        path = graph.find_path('a', 'b')
        assert path is None

        graph.create_edge('a', 'b')

        path = graph.find_path('a', 'b')
        assert path == ['a', 'b']

        graph.add_node('c')
        graph.create_edge('b', 'c')

        path = graph.find_path('a', 'c')
        assert path == ['a', 'b', 'c']

    def test_convert_path_to_edges(self):
        graph = Graph()
        graph.add_node('a')
        graph.add_node('b')
        graph.create_edge('a', 'b')

        path = graph.find_path('a', 'b')
        edges = graph.convert_path_to_edges(path)
        assert len(edges) == 1
        edge = edges[0]
        assert graph.get_edge_start(edge) == 'a'
        assert graph.get_edge_end(edge) == 'b'


class EqualityGraphTestCase(unittest.TestCase):
    def test_create_equality_edge(self):
        graph = EqualityGraph()
        graph.add_node('a')
        graph.add_node('b')

        reasoning_node_index = 5
        graph.create_equality_edge('a', 'b', reasoning_node_index)
        edge = graph.get_edge('a', 'b')
        assert isinstance(edge, EqualityEdge)
        assert edge.reasoning_node_index == reasoning_node_index


class LayoutTestCase(unittest.TestCase):
    def test_segment_ends(self):
        def f(*args):
            return len(args)
        segment = Segment.from_string('AB')
        assert f(*segment.ends) == 2

    def test_point_on_segment(self):
        relations = [statements.PointOnSegment(Point('B'), Segment.from_string('AC'))]
        r = Reasoning(relations)
        assert len(r.objects.registry[Segment]) == 3

    def test_new_segment(self):
        ab = Segment.from_string('AB')
        bc = Segment.from_string('BC')
        ac = Segment.from_string('AC')
        relations = [
            statements.SegmentsEqualityRelation(ab, bc),
            statements.SegmentsEqualityRelation(ab, ac),
        ]
        reasoning = Reasoning(relations)
        assert len(reasoning.objects.registry[Triangle]) == 1

    def test_angle_of_triangle(self):
        triangle = Triangle.from_string('ABC')
        self.assertTrue(
            statements.IsTriangleAngle(Angle.from_string('ABC'), triangle).is_tautology()
        )
        self.assertTrue(
            statements.IsTriangleAngle(Angle.from_string('CBA'), triangle).is_tautology()
        )
        self.assertTrue(
            statements.IsTriangleAngle(Angle.from_string('BAC'), triangle).is_tautology()
        )
        self.assertFalse(
            statements.IsTriangleAngle(Angle.from_string('ABD'), triangle).is_tautology()
        )

        r = Reasoning()

        assert r.get_justifier(
            statements.IsTriangleAngle(Angle.from_string('ABC'), triangle)
        )


class BisectrixDefinitionTestCase(unittest.TestCase):
    def test_args_layout(self):
        args_1 = (Segment.from_string('AD'), Angle.from_string('BAC'), Triangle.from_string('ABC'), )
        args_2 = (Segment.from_string('DA'), Angle.from_string('BAC'), Triangle.from_string('ABC'), )
        args_3 = (Segment.from_string('AD'), Angle.from_string('BAC'), Triangle.from_string('AMK'), )

        r = Reasoning()

        definition = statements.TriangleBisectrixDefinition

        args_layout_1 = definition.get_args_layout(*args_1)
        args_layout_2 = definition.get_args_layout(*args_2)
        args_layout_3 = definition.get_args_layout(*args_3)

        assert r.is_true(args_layout_1)
        assert r.is_true(args_layout_2)
        assert not r.is_true(args_layout_3)

    def test_aliases_layout(self):
        args_1 = (Segment.from_string('AD'), Angle.from_string('BAC'), Triangle.from_string('ABC'), )
        args_2 = (Segment.from_string('DA'), Angle.from_string('BAC'), Triangle.from_string('ABC'), )

        r = Reasoning()

        definition = statements.TriangleBisectrixDefinition

        aliases_layout_1 = definition.get_aliases_layout(*args_1)
        aliases_layout_2 = definition.get_aliases_layout(*args_2)

        assert r.is_true(aliases_layout_1)
        assert not r.is_true(aliases_layout_2)


class ReasoningTestCase(unittest.TestCase):
    def test_find_definition(self):
        args = (Segment.from_string('AD'), Angle.from_string('BAC'), Triangle.from_string('ABC'), )
        d = Reasoning.find_definition(
            statements.IsTriangleBisectrix(
                *args
            )
        )
        assert d == statements.TriangleBisectrixDefinition

    def test_find_all_args_by_type_and_layout(self):
        triangle = Triangle.from_string('ABC')

        relations = [
            statements.IsTriangleBisectrix(
                Segment.from_string('AK'),
                triangle.angle_at(Point('A')),
                triangle
            ),
            statements.IsTriangleBisectrix(
                Segment.from_string('BK'),
                triangle.angle_at(Point('B')),
                triangle
            ),
        ]

        r = Reasoning(relations=relations)

        res = r.find_all_args_by_type_and_layout(statements.IsoscelesTriangleDefinition)


class SimplifyObjectTestCase(unittest.TestCase):
    def test_id(self):
        domain = [Point('A')]
        res = FunctionCall(
            objects.PointsBijectionManager(domain).id(),
            Point('A')
        )
        assert res.eval() == Point('A')

    def test_function_call_eval_not_implemented(self):
        self.assertRaises(
            NotImplementedError,
            FunctionCall(
                Transformation('T'),
                Point('A')
            ).eval
        )

    def test_simplify_object(self):
        target_a = FunctionCall(Transformation('T'), Point('A'))
        target_b = FunctionCall(Transformation('T'), Point('B'))
        target_c = FunctionCall(Transformation('T'), Point('C'))
        triangle = Triangle(target_a, target_b, target_c)

        side_a = FunctionCall(objects.SideOppositeTo(triangle), target_a)

        assert Reasoning().equality_manager.simplify_object(side_a) == Segment(target_b, target_c)

        t = Transformation('T')

        relations = [
            statements.PointsEqualityRelation(Point('A2'), FunctionCall(t, Point('A1'))),
            statements.PointsEqualityRelation(Point('B2'), FunctionCall(t, Point('B1'))),
            statements.PointsEqualityRelation(Point('C2'), FunctionCall(t, Point('C1'))),
        ]
        reasoning = Reasoning(relations)

        assert reasoning.equality_manager.simplify_object(FunctionCall(t, Point('A1'))) == Point('A2')
        assert reasoning.equality_manager.simplify_object(
            Segment(
                FunctionCall(t, Point('A1')),
                FunctionCall(t, Point('B1'))
            )
        ) == Segment(Point('A2'), Point('B2'))
        assert reasoning.equality_manager.simplify_object(
            Triangle(
                FunctionCall(t, Point('A1')),
                FunctionCall(t, Point('B1')),
                FunctionCall(t, Point('C1'))
            )
        ) == Triangle(Point('A2'), Point('B2'), Point('C2'))


class SideAngleSideCongruenceTestCase(unittest.TestCase):
    def test_1(self):
        triangle_1 = Triangle(Point('A'), Point('B'), Point('C'))
        triangle_2 = Triangle(Point('D'), Point('E'), Point('F'))

        relations = [
            statements.AnglesEqualityRelation(triangle_1.angle_a, triangle_2.angle_a),
            statements.SegmentsEqualityRelation(triangle_1.side_b, triangle_2.side_b),
            statements.SegmentsEqualityRelation(triangle_1.side_c, triangle_2.side_c),
        ]
        to_derive = [
            statements.TrianglesEqualityRelation(triangle_1, triangle_2)
        ]

        r = Reasoning(relations=relations, to_derive=to_derive)
        r.go()

        assert r.done

    def test_2(self):
        triangle_1 = Triangle(Point('A'), Point('B'), Point('C'))
        triangle_2 = Triangle(Point('D'), Point('E'), Point('F'))

        to_derive = [
            statements.TrianglesEqualityRelation(triangle_1, triangle_2),
        ]

        r = Reasoning(
            relations=[
                statements.SegmentsEqualityRelation(triangle_1.side_a, triangle_2.side_a),
                statements.SegmentsEqualityRelation(triangle_1.side_b, triangle_2.side_b),
                statements.AnglesEqualityRelation(triangle_1.angle_c, triangle_2.angle_c),
            ],
            to_derive=to_derive
        )
        r.go()

        assert r.done

    def test_equal_triangles_features(self):
        theorem = statements.EqualTrianglesFeatures
        print unicode(theorem.apply(*theorem.sample_args))

    def test_triangle_bisectrix(self):
        point_a = objects.Point('A')
        point_b = objects.Point('B')
        point_c = objects.Point('C')
        point_d = objects.Point('D')

        bisectrix = objects.Segment(point_a, point_d)
        triangle = objects.Triangle(point_a, point_b, point_c)

        to_derive = [
            statements.AnglesEqualityRelation(
                objects.Angle(point_b, point_a, point_d),
                objects.Angle(point_c, point_a, point_d)
            )
        ]

        r = reasoning.Reasoning(
            relations=[
                statements.IsTriangleBisectrix(
                    bisectrix,
                    triangle.angle_a,
                    triangle
                )
            ],
            to_derive=to_derive
        )

        r.go()
        assert r.done

    def test_bisectrix_in_isosceles_triangle(self):
        point_a = objects.Point('A')
        point_b = objects.Point('B')
        point_c = objects.Point('C')
        point_d = objects.Point('D')

        bisectrix = objects.Segment(point_a, point_d)
        triangle = objects.Triangle(point_a, point_b, point_c)

        to_derive = [
            statements.TrianglesEqualityRelation(
                objects.Triangle(point_a, point_d, point_b),
                objects.Triangle(point_a, point_d, point_c)
            )
        ]

        r = reasoning.Reasoning(
            relations=[
                statements.IsTriangleBisectrix(
                    bisectrix,
                    triangle.angle_a,
                    triangle
                ),
                statements.SegmentsEqualityRelation(
                    triangle.side_b, triangle.side_c
                )
            ],
            to_derive=to_derive
        )

        r.go()

        assert r.done

    def test_matching_elements_in_equal_triangles(self):
        triangle_1 = Triangle(Point('A1'), Point('B1'), Point('C1'))
        triangle_2 = Triangle(Point('A2'), Point('B2'), Point('C2'))

        relations = [
            statements.SegmentsEqualityRelation(triangle_1.side_b, triangle_2.side_b),
            statements.SegmentsEqualityRelation(triangle_1.side_c, triangle_2.side_c),
            statements.AnglesEqualityRelation(triangle_1.angle_a, triangle_2.angle_a),
        ]
        to_derive = [
            statements.SegmentsEqualityRelation(triangle_1.side_a, triangle_2.side_a),
            # statements.AnglesEqualityRelation(triangle_1.angle_b, triangle_2.angle_b)
        ]
        r = reasoning.Reasoning(relations=relations, to_derive=to_derive)

        r.go()

        assert r.done

    def test_layout_for_equal_triangles_features(self):
        triangle_1 = Triangle.from_string('ABC')
        triangle_2 = Triangle.from_string('DEF')
        args = (triangle_1, triangle_2, Transformation('T'),
                objects.PointsBijectionManager(triangle_2.points).id())
        r = Reasoning()
        print unicode(statements.EqualTrianglesFeatures.get_args_layout(*args))
        assert r.is_true(statements.EqualTrianglesFeatures.get_args_layout(*args))

    def test_set__in(self):
        assert Point('A') not in {Point('A')}
        assert Point('A') in [Point('A')]
        assert [Segment.from_string('AB')] == [Segment.from_string('AB')]

        t1 = Transformation('T')
        t2 = Transformation('T')
        assert t1 == t2

        call_1 = FunctionCall(t1, Point('A'))
        call_2 = FunctionCall(t2, Point('A'))
        assert call_1 == call_2

        assert call_1.complexity == 2

    def test_cmp_key(self):
        triangle_1 = Triangle.from_string('ABC')
        triangle_2 = Triangle.from_string('DEF')

        assert statements.SideAngleSideCongruenceTheorem.get_cmp_key([triangle_1, triangle_2]) == \
            statements.SideAngleSideCongruenceTheorem.get_cmp_key([triangle_2, triangle_1])

        triangle_1_v1 = Triangle.from_string('ABC')
        triangle_1_v2 = Triangle.from_string('CAB')
        triangle_2_v1 = Triangle.from_string('DEF')
        triangle_2_v2 = Triangle.from_string('FDE')

        bijection_1 = objects.PointsBijectionManager(triangle_2_v1.points).id()
        bijection_2 = objects.PointsBijectionManager(triangle_2_v2.points).id()

        assert bijection_1 == bijection_2

        key_1 = [statements.EqualTrianglesByTransformationDefinition,
                 [triangle_1_v1, triangle_2_v1, Transformation('T'), bijection_1]]

        key_2 = [statements.EqualTrianglesByTransformationDefinition,
                 [triangle_1_v2, triangle_2_v2, Transformation('T'), bijection_2]]

        assert key_1 == key_2

    def test_simplify_relation(self):
        t = Transformation('T')

        relations = [
            statements.PointsEqualityRelation(Point('A2'), FunctionCall(t, Point('A1'))),
            statements.PointsEqualityRelation(Point('B2'), FunctionCall(t, Point('B1'))),
        ]
        reasoning = Reasoning(relations)

        res = reasoning.equality_manager.simplify_relation(
            statements.PointsEqualityRelation(
                FunctionCall(t, Point('A1')),
                FunctionCall(t, Point('B1'))
            )
        )[1]
        assert res == statements.PointsEqualityRelation(Point('A2'), Point('B2'))

        res = reasoning.equality_manager.simplify_relation(
            statements.SegmentsEqualityRelation(
                Segment(
                    FunctionCall(t, Point('A1')),
                    FunctionCall(t, Point('B1'))
                ),
                Segment(Point('A1'), Point('B1'))
            )
        )[1]

        assert res == statements.SegmentsEqualityRelation(
            Segment(Point('A1'), Point('B1')),
            Segment(Point('A2'), Point('B2'))
        )

    def test_pick_unique_names(self):
        reasoning = Reasoning()
        obj = reasoning.objects.pick_unique_names(
            [objects.Transformation('T')]
        )[0]
        assert unicode(obj.name) == 'T'

        reasoning.objects.add_object(objects.Transformation('T'))
        obj = reasoning.objects.pick_unique_names(
            [objects.Transformation('T')]
        )[0]
        assert unicode(obj.name) == 'T1'


class AngleMeasureTestCase(unittest.TestCase):
    def test_angles_value(self):
        relations = [
            statements.AlgebraicEqualityRelation(
                FunctionCall(AngleMeasure(), Angle.from_string('ABC')),
                MeasureResult(90, 'degree')
            ),
            statements.AlgebraicEqualityRelation(
                FunctionCall(AngleMeasure(), Angle.from_string('DEF')),
                MeasureResult(90, 'degree')
            ),
        ]
        to_derive = [
            # statements.AlgebraicEqualityRelation(
            #     FunctionCall(AngleMeasure(), Angle.from_string('ABC')),
            #     FunctionCall(AngleMeasure(), Angle.from_string('DEF')),
            # ),
            statements.AnglesEqualityRelation(
                Angle.from_string('ABC'),
                Angle.from_string('DEF')
            )
        ]
        r = Reasoning(relations=relations, to_derive=to_derive)
        r.go()

        assert r.done

    def test_side_angle_side_same_triangle(self):
        r = Reasoning()
        triangle = Triangle.from_string('ABC')
        assert r.theorem_gives_tautology(statements.SideAngleSideCongruenceTheorem, [triangle, triangle])

    def test_theorem_manager(self):
        mgr = reasoning.TheoremManager()
        mgr.add_theorems([statements.TriangleBisectrixDefinition])
        assert len(mgr.all()) == 2

    def test_equal_triangles_by_angles_value(self):
        relations = [
            statements.AlgebraicEqualityRelation(
                FunctionCall(AngleMeasure(), Angle.from_string('ABC')),
                MeasureResult(90, 'degree')
            ),
            statements.AlgebraicEqualityRelation(
                FunctionCall(AngleMeasure(), Angle.from_string('DEF')),
                MeasureResult(90, 'degree')
            ),
            statements.SegmentsEqualityRelation(
                Segment.from_string('AB'), Segment.from_string('DE')
            ),
            statements.SegmentsEqualityRelation(
                Segment.from_string('BC'), Segment.from_string('EF')
            ),
        ]

        to_derive = [
            statements.TrianglesEqualityRelation(
                Triangle.from_string('ABC'), Triangle.from_string('DEF')
            )
        ]
        r = reasoning.Reasoning(relations=relations, to_derive=to_derive)

        r.go()

        assert r.done

    def test_equal_triangles_by_angles_value_2(self):
        relations = [
            statements.AlgebraicEqualityRelation(
                FunctionCall(AngleMeasure(), Angle.from_string('ACD')),
                MeasureResult(90, 'degree')
            ),
            statements.AlgebraicEqualityRelation(
                FunctionCall(AngleMeasure(), Angle.from_string('BDC')),
                MeasureResult(90, 'degree')
            ),
            statements.SegmentsEqualityRelation(
                Segment.from_string('AC'), Segment.from_string('BD')
            ),
        ]
        to_derive = [
            statements.TrianglesEqualityRelation(
                Triangle.from_string('ADC'), Triangle.from_string('BCD')
            )
        ]
        r = reasoning.Reasoning(relations=relations, to_derive=to_derive)
        r.go()

        assert r.done

    def test_equal_triangles_by_angles_value_3(self):
        relations = [
            statements.AlgebraicEqualityRelation(
                FunctionCall(AngleMeasure(), Angle.from_string('ABC')),
                MeasureResult(90, 'degree')
            ),
            statements.AlgebraicEqualityRelation(
                FunctionCall(AngleMeasure(), Angle.from_string('DEF')),
                MeasureResult(90, 'degree')
            ),
            statements.SegmentsEqualityRelation(
                Segment.from_string('AB'), Segment.from_string('DE')
            ),
            statements.SegmentsEqualityRelation(
                Segment.from_string('BC'), Segment.from_string('EF')
            ),
        ]

        to_derive = [
            statements.SegmentsEqualityRelation(
                Segment.from_string('AC'), Segment.from_string('DF')
            )
        ]
        r = reasoning.Reasoning(relations=relations, to_derive=to_derive)

        r.go()

        assert r.done


class SegmentLengthTestCase(unittest.TestCase):
    def test_measure_unit_conversion(self):
        converter = statements.MeasureUnitConverter()
        assert converter.convert(MeasureResult(Decimal(5), 'cm'), 'mm') == MeasureResult(Decimal(50), 'mm')
        assert converter.convert(MeasureResult(Decimal(5), 'mm'), 'cm') == MeasureResult(Decimal('0.5'), 'cm')

    def test_simplify_relation(self):
        relations = [
            statements.PointOnSegment(Point('B'), Segment.from_string('AC')),
            AlgebraicEqualityRelation(
                FunctionCall(SegmentLength(), Segment.from_string('AB')),
                MeasureResult(Decimal(2), 'cm')
            ),
            AlgebraicEqualityRelation(
                FunctionCall(SegmentLength(), Segment.from_string('BC')),
                MeasureResult(Decimal(3), 'cm')
            )
        ]
        r = Reasoning(relations=relations)

        assert r.equality_manager.simplify_object(
            FunctionCall(SegmentLength(), Segment.from_string('AB'))
        ) == MeasureResult(Decimal(2), 'cm')

        simplified_sum = r.equality_manager.simplify_object(
            AlgebraicExpression(
                '+',
                [FunctionCall(SegmentLength(), Segment.from_string('AB')),
                 FunctionCall(SegmentLength(), Segment.from_string('BC')), ]
            )
        )
        assert simplified_sum == AlgebraicExpression(
            '+', [MeasureResult(Decimal(2), 'cm'), MeasureResult(Decimal(3), 'cm')]
        )

    def test_point_on_segment(self):
        relations = [
            statements.PointOnSegment(Point('B'), Segment.from_string('AC')),
            AlgebraicEqualityRelation(
                FunctionCall(SegmentLength(), Segment.from_string('AB')),
                MeasureResult(Decimal(2), 'cm')
            ),
            AlgebraicEqualityRelation(
                FunctionCall(SegmentLength(), Segment.from_string('BC')),
                MeasureResult(Decimal(3), 'cm')
            ),
            # AlgebraicEqualityRelation(
            #     FunctionCall(SegmentLength(), Segment.from_string('AC')),
            #     MeasureResult(Variable('x'), 'cm')
            # )
        ]
        x = FunctionCall(SegmentLength(), Segment.from_string('AC'))
        to_calculate = [x]

        r = Reasoning(relations=relations, to_calculate=to_calculate)
        r.go()

        assert r.get_calculated_value(x) == MeasureResult(5, 'cm')


class LinearEquationSolverTestCase(unittest.TestCase):
    def test_expression_to_unicode(self):
        assert ExpressionParser().to_unicode(
            Variable('x')
        ) == 'x'

        assert ExpressionParser().to_unicode(
            ConstAlgebraicExpression(1)
        ) == '1'

        assert ExpressionParser().to_unicode(
            AlgebraicExpression(operator='+', args=[Variable('x'), ConstAlgebraicExpression(1)])
        ) == 'x + 1'

        assert ExpressionParser().to_unicode(
            AlgebraicExpression(operator='-', args=[Variable('x'), ConstAlgebraicExpression(1)])
        ) == 'x - 1'

        assert ExpressionParser().to_unicode(
            AlgebraicExpression(operator='-', args=[Variable('x')])
        ) == '-x'

        assert ExpressionParser().to_unicode(
            AlgebraicExpression('-', args=[Variable('x'), AlgebraicExpression('+', [Variable('x'), Variable('y')])])
        ) == 'x - (x + y)'

    def test_simplify_expression(self):
        equation = AlgebraicEqualityRelation(Variable('x'), ConstAlgebraicExpression(1))
        solver = LinearEquationSolver(equation)

        expression = ConstAlgebraicExpression(1)
        assert (False, expression) == solver.simplify_expression(expression)

        expression = AlgebraicExpression(
            '+',
            [ConstAlgebraicExpression(2), ConstAlgebraicExpression(3)]
        )
        status, simplified_expression = solver.simplify_expression(expression)
        assert status is True and simplified_expression == ConstAlgebraicExpression(5)

        expression = AlgebraicExpression(
            '-',
            [ConstAlgebraicExpression(7), ConstAlgebraicExpression(4)]
        )
        status, simplified_expression = solver.simplify_expression(expression)
        assert status is True and simplified_expression == ConstAlgebraicExpression(3)

        expression = AlgebraicExpression(
            '+',
            [Variable('x'), ConstAlgebraicExpression(2)]
        )
        status, simplified_expression = solver.simplify_expression(expression)
        assert status is False and simplified_expression == expression

        expression = AlgebraicExpression(
            '-',
            [Variable('x'), ConstAlgebraicExpression(4)]
        )
        status, simplified_expression = solver.simplify_expression(expression)
        assert status is False and simplified_expression == expression

    def test_equation_solve(self):
        equation = AlgebraicEqualityRelation(Variable('x'), ConstAlgebraicExpression(1))
        solver = LinearEquationSolver(equation)
        res = solver.solve(Variable('x'))
        assert res == ConstAlgebraicExpression(1)

        equation = AlgebraicEqualityRelation(ConstAlgebraicExpression(1), Variable('x'))
        solver = LinearEquationSolver(equation)
        res = solver.solve(Variable('x'))
        assert res == ConstAlgebraicExpression(1)

        equation = AlgebraicEqualityRelation(
            Variable('x'),
            AlgebraicExpression('+', [ConstAlgebraicExpression(2), ConstAlgebraicExpression(3)])
        )
        solver = LinearEquationSolver(equation)
        res = solver.solve(Variable('x'))
        assert res == ConstAlgebraicExpression(5)

        equation = AlgebraicEqualityRelation(
            Variable('x'),
            AlgebraicExpression('-', [ConstAlgebraicExpression(2), ConstAlgebraicExpression(6)])
        )
        solver = LinearEquationSolver(equation)
        res = solver.solve(Variable('x'))
        assert res == ConstAlgebraicExpression(-4)

        equation = AlgebraicEqualityRelation(
            AlgebraicExpression('+', [Variable('x'), ConstAlgebraicExpression(1)]),
            ConstAlgebraicExpression(4)
        )
        solver = LinearEquationSolver(equation)
        res = solver.solve(Variable('x'))
        assert res == ConstAlgebraicExpression(3)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SideAngleSideCongruenceTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
