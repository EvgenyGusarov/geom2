# coding=utf-8

from __future__ import unicode_literals
import json
import itertools
from itertools import permutations
import operator
from decimal import Decimal

from objects import short_unicode, FiniteSet, OrderedFiniteSet, FunctionCall, \
    Permutation, PermutationManager, sets_equal, \
    Point, Angle, Segment, Triangle, \
    AlgebraicExpression, Variable, MeasureResult, AngleMeasure, ConstAlgebraicExpression, Transformation, \
    ValidationError, check_args_are_of_type, PointsBijection, PointsBijectionManager, SegmentLength, \
    ExpressionParser


class StatementBase(object):
    def walk(self, handler):
        raise NotImplementedError


class LogicStatement(StatementBase):
    logic_op = None
    logic_items = None

    def __init__(self, logic_op, *logic_items):
        self.logic_op = logic_op

        if logic_op in ('exists', 'for_each'):
            self.quantor_args = logic_items[0]
            self.logic_items = logic_items[1:]
        else:
            self.quantor_args = []
            self.logic_items = list(logic_items)

    def apply(self, *args, **kwargs):
        return self.__class__(self.logic_op, map(lambda x: x.apply(*args, **kwargs), self.logic_items))

    def __unicode__(self):
        if self.logic_op in ('and', 'or'):
            if len(self.logic_items) == 0:
                return u'(Empty statement)'
            else:
                items = map(unicode, self.logic_items)
                if len(self.logic_items) > 1:
                    items = map(lambda s: u'({})'.format(s), items)
                op = u' {} '.format(self.logic_op)
                items = op.join(items)
                return u'[{}]'.format(items)
        elif self.logic_op in ('=>', '<=>'):
            return u'{} {} {}'.format(self.logic_items[0], self.logic_op, self.logic_items[1])
        elif self.logic_op in ('exists', 'for_each'):
            return u'{} ({}) such that {}'.format(
                self.logic_op, ', '.join(map(unicode, self.quantor_args)), self.logic_items[0])

    def walk(self, handler):
        res = []

        for item in self.logic_items:
            if isinstance(item, LogicStatement):
                res += item.walk(handler)
            elif isinstance(item, Relation):
                res.append(handler(item))

        return res


class Relation(StatementBase):
    arg_types = None

    def __init__(self, *args):
        if self.arg_types:
            assert len(args) == len(self.arg_types), u'Wrong number of args'

            assert check_args_are_of_type(args, self.arg_types), \
                u'args {} have wrong type, expected {}'.format(
                        ', '.join(map(unicode, args)),
                        ', '.join(map(unicode, self.arg_types))
                )

        self.args = list(args)

    def apply(self, *args, **kwargs):
        args = [kwargs[key] for key in self.args]
        return self.__class__(*args)

    def __eq__(self, other):
        return self.__class__ == other.__class__ and self.args == other.args

    def is_tautology(self):
        return False

    def walk(self, handler):
        return [handler(self)]

    def is_true(self):
        return None


class EquivalenceRelation(Relation):
    def __unicode__(self):
        return u'{} ~ {}'.format(*self.args)


def iter_matches(args_1, args_2):
    for permutation in permutations(args_1):
        yield zip(permutation, args_2)


class EqualityRelation(EquivalenceRelation):
    def __unicode__(self):
        return u'{} = {}'.format(*self.args)

    def __eq__(self, other):
        if type(self) != type(other):
            return False
        else:
            return sets_equal(self.args, other.args)

    def is_tautology(self):
        return self.args[0] == self.args[1]

    def is_true(self):
        if self.is_tautology():
            return True
        else:
            return None


self_equivalence_statement = LogicStatement(
    '=>',
    [EqualityRelation('x', 'y'), EquivalenceRelation('x', 'y'), ]
)

symmetric_equivalence_statement = LogicStatement(
    '=>',
    [EquivalenceRelation('x', 'y'), EquivalenceRelation('y', 'x'), ]
)

transitive_equivalence_statement = LogicStatement(
    '=>',
    [
        LogicStatement('and', [EquivalenceRelation('x', 'y'), EquivalenceRelation('y', 'z'), ]),
        EquivalenceRelation('x', 'z'),
    ]
)


class FloatEqualityRelation(EqualityRelation):
    arg_types = (float, float)

    def is_true(self):
        return self.args[0] == self.args[1]


class PointsEqualityRelation(EqualityRelation):
    arg_types = (Point, Point)

    def is_true(self):
        return self.args[0] == self.args[1]


class SegmentsEqualityRelation(EqualityRelation):
    arg_types = (Segment, Segment)


class AnglesEqualityRelation(EqualityRelation):
    arg_types = (Angle, Angle)


class TrianglesEqualityRelation(EqualityRelation):
    arg_types = (Triangle, Triangle)


class IsSegment(Relation):
    arg_types = (Segment, Point, Point)

    def __unicode__(self):
        return u'{} = segment {}{}'.format(map(lambda x: x.name, self.args))


class PointOnSegment(Relation):
    arg_types = (Point, Segment)

    def __unicode__(self):
        return u'{} lies on {}'.format(*self.args)

    def is_tautology(self):
        point, segment = self.args
        return point == segment.end_1 or point == segment.end_2


class IsTriangle(Relation):
    arg_types = (Triangle, Point, Point, Point)

    def __unicode__(self):
        triangle, a, b, c = self.args
        return u'{} = triangle {}{}{}'.format(triangle.name, a.name, b.name, c.name)


class IsPointOfTriangle(Relation):
    arg_types = (Point, Triangle)

    def is_true(self):
        point, triangle = self.args
        return point in triangle.points

    def __unicode__(self):
        return u'{} is a point of {}'.format(*self.args)


class IsAngle(Relation):
    arg_types = (Angle, Point, Point, Point)

    def __unicode__(self):
        return u'{} = angle {}{}{}'.format(*self.args)


class IsTriangleAngle(Relation):
    arg_types = (Angle, Triangle, )

    def is_tautology(self):
        angle, triangle = self.args
        return sets_equal(angle.points_and_vertex, triangle.points)

    def __unicode__(self):
        return u'{} is angle of {}'.format(*self.args)


class IsTriangleBisectrix(Relation):
    arg_types = (Segment, Angle, Triangle)

    def __unicode__(self):
        return u'{} is bisectrix of {} in {}'.format(*self.args)


class InvalidLayout(Exception):
    pass


class MultipleAlternatives(Exception):
    pass

# Definitions are statement factories


class StatementFactory(object):
    arg_types = None
    sample_args = None
    subject = None
    # consequence_type = None
    verbose_name = None
    is_reversible = False

    @staticmethod
    def get_args_layout(*args):
        # This method must be alias-independent: either aliases you pass, result must be unchanged.
        # Use get_aliases_layout for aliases-dependent statements.
        # Reasoning.is_true should return True on empty layout
        return LogicStatement('and')

    @staticmethod
    def get_aliases_layout(*aliases):
        return LogicStatement('and')

    @staticmethod
    def apply(*args):
        return LogicStatement('=>')

    @staticmethod
    def get_consequence_args_layout(*args):
        return LogicStatement('and')

    @staticmethod
    def get_args(*consequence_args):
        raise MultipleAlternatives

    @classmethod
    def get_consequence_args(cls, *args):
        applied = cls.apply(*args)
        consequence = applied.logic_items[1]

        if isinstance(consequence, Relation):
            return consequence.args
        elif isinstance(consequence, LogicStatement):
            return (x.args for x in consequence.logic_items)

    # @staticmethod
    # def args_are_reasonable(*args):
    #     return True

    @classmethod
    def get_cmp_key(cls, args):
        return cls, args


class AnglesEqualityCriterion(StatementFactory):
    arg_types = (Angle, Angle)
    sample_args = (Angle.from_string('ABC'), Angle.from_string('DEF'))
    verbose_name = 'angles equality criterion'
    is_reversible = True

    @classmethod
    def get_cmp_key(cls, args):
        return AnglesEqualityRelation(*args)

    @staticmethod
    def apply(*args):
        angle_1, angle_2 = args

        return LogicStatement(
            '<=>',
            AnglesEqualityRelation(*args),
            AlgebraicEqualityRelation(
                FunctionCall(AngleMeasure(), angle_1),
                FunctionCall(AngleMeasure(), angle_2)
            )
        )


class TriangleBisectrixDefinition(StatementFactory):
    arg_types = (Segment, Angle, Triangle)
    sample_args = (Segment.from_string('AD'), Angle.from_string('BAC'), Triangle.from_string('ABC'))
    subject = IsTriangleBisectrix
    verbose_name = u'triangle bisectrix definition'
    is_reversible = True

    @staticmethod
    def get_args_layout(*args):
        segment, angle, triangle = args
        return LogicStatement(
            'and',
            PointIsSetElement(angle.vertex, segment.ends),
            IsTriangleAngle(angle, triangle)
        )

    @staticmethod
    def get_aliases_layout(*aliases):
        segment, angle, triangle = aliases
        return PointsEqualityRelation(angle.vertex, segment.end_1)

    @staticmethod
    def get_consequence_args_layout(*args):
        angle_1, angle_2 = args

        return LogicStatement(
            'and',
            PointsEqualityRelation(angle_1.vertex, angle_2.vertex),
            AlgebraicEqualityRelation(len(FiniteSet(*angle_1.points) & FiniteSet(*angle_2.points)), 1)
        )

    @staticmethod
    def apply(*args):
        segment, angle, triangle = args

        return LogicStatement(
            '<=>',
            IsTriangleBisectrix(segment, angle, triangle),
            LogicStatement(
                'and',
                AnglesEqualityRelation(
                    *angle.divide_by_segment(segment)
                ),
                PointOnSegment(segment.end_2, triangle.side_opposite_to(angle.vertex))
            )
        )


class TransformationDefinition(StatementFactory):
    arg_types = (Transformation, Segment, Segment)

    @staticmethod
    def apply(*args):
        transformation, source_segment, target_segment = args
        return LogicStatement(
            '=>',
            LogicStatement(
                'and',
                PointsEqualityRelation(
                    FunctionCall(transformation, source_segment.point_a), target_segment.point_a
                ),
                PointsEqualityRelation(
                    FunctionCall(transformation, source_segment.point_b), target_segment.point_b
                )
            ),
            SegmentsEqualityRelation(source_segment, target_segment)
        )


def check_lists_are_exact(list_1, list_2):
    # NEED RESTORE
    return False


class ExistsFactory(StatementFactory):
    arg_types = tuple()
    sample_args = tuple()
    consequence_factory_type = None
    inherited_args = tuple()

    @staticmethod
    def build_consequence_factory_args(*args):
        return args

    def is_partial_of(self, other):
        if not isinstance(other, ExistsFactory):
            return False

        if self.consequence_factory_type != other.consequence_factory_type:
            return False
        if not check_lists_are_exact(self.inherited_args, other.inherited_args):
            return False
        res = len(self.arg_types) <= len(other.arg_types)

        return res

    def __unicode__(self):
        return unicode(self.apply(*self.sample_args))


class EqualTrianglesDefinition(StatementFactory):
    arg_types = (Triangle, Triangle)
    sample_args = (Triangle.from_string('ABC'), Triangle.from_string('DEF'))
    subject = TrianglesEqualityRelation
    verbose_name = u'Equal triangles definition'

    # @staticmethod
    # def args_are_reasonable(*args):
    #     triangle_1, triangle_2 = args
    #     return triangle_1 != triangle_2

    @staticmethod
    def apply(*args):
        triangle_1, triangle_2 = args

        class ArgsExistsFactory(ExistsFactory):
            arg_types = (Transformation, PointsBijection)
            sample_args = (Transformation('T'), PointsBijectionManager(triangle_2.points).get_object_by_name('P'))
            consequence_factory_type = EqualTrianglesByTransformation
            inherited_args = (triangle_1, triangle_2, )
            is_reversible = True

            @staticmethod
            def build_consequence_factory_args(*quantor_args):
                return (triangle_1, triangle_2, ) + quantor_args

            @staticmethod
            def apply(*quantor_args):
                transformation, permutation = quantor_args

                return LogicStatement(
                    'exists',
                    quantor_args,
                    EqualTrianglesByTransformation(
                        triangle_1, triangle_2, transformation, permutation
                    )
                )

            def __unicode__(self):
                transformation = self.sample_args[0]
                return u'exists {} such that {}({}) = {}'.format(
                    transformation, transformation.name,
                    triangle_1, triangle_2
                )

        return LogicStatement(
            '<=>',
            TrianglesEqualityRelation(triangle_1, triangle_2),
            ArgsExistsFactory()
        )


class EqualTrianglesByTransformation(Relation):
    arg_types = (Triangle, Triangle, Transformation, PointsBijection)

    def __unicode__(self):
        # triangle_1, triangle_2, transformation, permutation = self.args

        return '{}={} using {} and {}'.format(*self.args)

        # return ','.join([
        #     u'{}({}) = {}'.format(transformation.name, triangle_1, triangle_2),
        #     u'{} matches {}'.format(triangle_1.point_a, triangle_2.point_a),
        #     u'{} matches {}'.format(triangle_1.point_b, triangle_2.point_b),
        #     u'{} matches {}'.format(triangle_1.point_c, triangle_2.point_c)
        # ])


class EqualTrianglesByTransformationDefinition(StatementFactory):
    arg_types = (Triangle, Triangle, Transformation, PointsBijection)
    sample_args = (Triangle.from_string('ABC'), Triangle.from_string('DEF'), Transformation('T'),
                   PointsBijectionManager(FiniteSet(Point('D'), Point('E'), Point('F'))).id())
    subject = EqualTrianglesByTransformation
    verbose_name = 'equal triangles by transformation definition'
    is_reversible = True

    @staticmethod
    def apply(*args):
        source_triangle, target_triangle, transformation, permutation = args

        return LogicStatement(
            '<=>',
            EqualTrianglesByTransformation(*args),
            LogicStatement(
                'and',
                PointsEqualityRelation(
                    FunctionCall(transformation, source_triangle.point_a),
                    FunctionCall(permutation, target_triangle.point_a)
                ),
                PointsEqualityRelation(
                    FunctionCall(transformation, source_triangle.point_b),
                    FunctionCall(permutation, target_triangle.point_b)
                ),
                PointsEqualityRelation(
                    FunctionCall(transformation, source_triangle.point_c),
                    FunctionCall(permutation, target_triangle.point_c)
                )
            )
        )


class EqualTrianglesFeatures(StatementFactory):
    arg_types = (Triangle, Triangle, Transformation, PointsBijection)
    sample_args = (
        Triangle.from_string('ABC'), Triangle.from_string('DEF'), Transformation('T'),
        PointsBijectionManager(Triangle.from_string('DEF').points).id()
    )
    condition_class = EqualTrianglesByTransformation
    verbose_name = u'equal triangles features'

    @staticmethod
    def build_condition_args(*args):
        return args

    @staticmethod
    def get_args_layout(*args):
        triangle_1, triangle_2, transformation, bijection = args
        return LogicStatement(
            'and',
            SetsEqualityRelation(triangle_2.points, bijection.domain),
            SetsEqualityRelation(triangle_2.points, bijection.codomain),
        )

    @staticmethod
    def apply(*args):
        source_triangle, target_triangle, transformation, permutation = args

        target_triangle = Triangle(
            FunctionCall(transformation, source_triangle.point_a),
            FunctionCall(transformation, source_triangle.point_b),
            FunctionCall(transformation, source_triangle.point_c)
        )

        return LogicStatement(
            '=>',
            EqualTrianglesByTransformation(*args),
            LogicStatement(
                'and',
                SegmentsEqualityRelation(
                    source_triangle.side_a,
                    target_triangle.side_opposite_to(permutation(target_triangle.point_a))
                ),
                SegmentsEqualityRelation(
                    source_triangle.side_b,
                    target_triangle.side_opposite_to(permutation(target_triangle.point_b))
                ),
                SegmentsEqualityRelation(
                    source_triangle.side_c,
                    target_triangle.side_opposite_to(permutation(target_triangle.point_c))
                ),
                AnglesEqualityRelation(
                    source_triangle.angle_a,
                    target_triangle.angle_at(permutation(target_triangle.point_a))
                ),
                AnglesEqualityRelation(
                    source_triangle.angle_b,
                    target_triangle.angle_at(permutation(target_triangle.point_b))
                ),
                AnglesEqualityRelation(
                    source_triangle.angle_c,
                    target_triangle.angle_at(permutation(target_triangle.point_c))
                )
            )
        )


class SideAngleSideCongruenceTheorem(StatementFactory):
    arg_types = (Triangle, Triangle, )
    sample_args = (Triangle.from_string('ABC'), Triangle.from_string('DEF'))
    verbose_name = u'side-angle-side congruence theorem'

    # @staticmethod
    # def args_are_reasonable(*args):
    #     triangle_1, triangle_2 = args
    #     return triangle_1 != triangle_2

    @staticmethod
    def apply(*args):
        triangle_1, triangle_2 = args
        permutation = PointsBijectionManager(triangle_2.points).id()

        class ArgsExistsFactory(ExistsFactory):
            arg_types = (Transformation, )
            sample_args = (Transformation('T'), )
            consequence_factory_type = EqualTrianglesByTransformation
            inherited_args = (triangle_1, triangle_2, )

            @staticmethod
            def build_consequence_factory_args(*quantor_args):
                return triangle_1, triangle_2, quantor_args[0], permutation

            @staticmethod
            def apply(*quantor_args):
                transformation = quantor_args[0]

                return LogicStatement(
                    'exists',
                    (transformation, ),
                    EqualTrianglesByTransformation(triangle_1, triangle_2, transformation, permutation)
                )

            # def __unicode__(self):
            #     transformation = self.sample_args[0]`````
            #     return u'exists {} such that [{}, {}, {}, {}]'.format(
            #         transformation,
            #         u'{}({}) = {}'.format(transformation.name, triangle_1, triangle_2),
            #         u'{} matches {}'.format(triangle_1.point_a, triangle_2.point_a),
            #         u'{} matches {}'.format(triangle_1.point_b, triangle_2.point_b),
            #         u'{} matches {}'.format(triangle_1.point_c, triangle_2.point_c)
            #     )

        return LogicStatement(
            '=>',
            LogicStatement(
                'and',
                AnglesEqualityRelation(triangle_1.angle_a, triangle_2.angle_a),
                SegmentsEqualityRelation(triangle_1.side_b, triangle_2.side_b),
                SegmentsEqualityRelation(triangle_1.side_c, triangle_2.side_c)
            ),
            LogicStatement(
                'and',
                TrianglesEqualityRelation(triangle_1, triangle_2),
                ArgsExistsFactory()
            )
        )

    @classmethod
    def get_cmp_key(cls, args):
        return TrianglesEqualityRelation(*args)


class IsoscelesTriangle(Relation):
    arg_types = (Triangle, )

    def __unicode__(self):
        return u'{} is isosceles'.format(self.args[0])


class IsoscelesTriangleDefinition(StatementFactory):
    arg_types = (Triangle, )
    sample_args = (Triangle.from_string('ABC'), )
    subject = IsoscelesTriangle
    verbose_name = u'isosceles triangle definition'
    is_reversible = True

    @staticmethod
    def get_consequence_args_layout(*args):
        side_1, side_2 = args
        return AlgebraicEqualityRelation(len(FiniteSet(*side_1.ends) & FiniteSet(*side_2.ends)), 1)

    @staticmethod
    def apply(*args):
        triangle = args[0]

        return LogicStatement(
            '<=>',
            IsoscelesTriangle(triangle),
            SegmentsEqualityRelation(triangle.side_a, triangle.side_b)
        )


class IsoscelesTriangleEqualAnglesProperty(StatementFactory):
    arg_types = (Triangle, )
    sample_args = (Triangle.from_string('ABC'), )
    # consequence_type = AnglesEqualityRelation
    verbose_name = 'isosceles triangle equal angles property'

    @staticmethod
    def apply(*args):
        triangle = args[0]

        return LogicStatement(
            '=>',
            IsoscelesTriangle(triangle),
            AnglesEqualityRelation(triangle.angle_a, triangle.angle_b)
        )

    @staticmethod
    def get_consequence_args_layout(*args):
        angle_a, angle_b = args

        return LogicStatement(
            'and',
            SetsEqualityRelation(FiniteSet(*angle_a.points_and_vertex), FiniteSet(*angle_b.points_and_vertex)),
            LogicStatement('not', PointsEqualityRelation(angle_a.vertex, angle_b.vertex))
        )

    @staticmethod
    def get_args_from_consequence_args(*consequence_args):
        return Triangle(*consequence_args[0].points_and_vertex)


class CompoundSegmentLengthProperty(StatementFactory):
    arg_types = (Point, Point, Point)
    sample_args = (Point('A'), Point('B'), Point('C'))
    verbose_name = 'compound segment length property'

    @staticmethod
    def apply(*args):
        point_a, point_b, point_c = args

        return LogicStatement(
            '=>',
            PointOnSegment(point_b, Segment(point_a, point_c)),
            AlgebraicEqualityRelation(
                FunctionCall(SegmentLength(), Segment(point_a, point_c)),
                AlgebraicExpression(
                    '+',
                    [FunctionCall(SegmentLength(), Segment(point_a, point_b)),
                     FunctionCall(SegmentLength(), Segment(point_b, point_c)), ]
                )
            )
        )

    @classmethod
    def get_cmp_key(cls, args):
        point_a, point_b, point_c = args
        return PointOnSegment(point_b, Segment(point_a, point_c))


axioms = (
    self_equivalence_statement,
    symmetric_equivalence_statement,
    transitive_equivalence_statement,
)

basic_geom_relations = (IsSegment, IsAngle, IsTriangle, IsTriangleBisectrix, )

definitions = (
    # EqualTrianglesDefinition,
    IsoscelesTriangleDefinition,
    TriangleBisectrixDefinition,
    EqualTrianglesByTransformationDefinition,
)

theorems = (
    EqualTrianglesFeatures,
    SideAngleSideCongruenceTheorem,
    IsoscelesTriangleEqualAnglesProperty,
    AnglesEqualityCriterion,
    CompoundSegmentLengthProperty,
)


def iter_segment_aliases(segment):
    for points in permutations(segment.ends, len(segment.ends)):
        yield Segment(*points)


def iter_angle_aliases(angle):
    for points in permutations(angle.points):
        yield Angle(points[0], angle.vertex, points[1])


def iter_triangle_aliases(triangle):
    for points in itertools.permutations(triangle.points, len(triangle.points)):
        yield Triangle(*points)


def iter_object_aliases(obj):
    if isinstance(obj, Segment):
        for alias in iter_segment_aliases(obj):
            yield alias
    elif isinstance(obj, Angle):
        for alias in iter_angle_aliases(obj):
            yield alias
    elif isinstance(obj, Triangle):
        for alias in iter_triangle_aliases(obj):
            yield alias
    else:
        yield obj


def iter_aliases_tuples(*args):
    aliases = (list(iter_object_aliases(arg)) for arg in args)
    return itertools.product(*aliases)


class SetsEqualityRelation(EqualityRelation):
    arg_types = (FiniteSet, FiniteSet)


class PointIsSetElement(Relation):
    arg_types = (Point, FiniteSet)

    def is_tautology(self):
        point, set = self.args
        return point in set

    def __unicode__(self):
        return '{} is element of {}'.format(*self.args)


class AlgebraicEqualityRelation(EqualityRelation):
    arg_types = (AlgebraicExpression, AlgebraicExpression)

    def __init__(self, *args):
        args = list(args)

        for i, arg in enumerate(args):
            if isinstance(arg, (int, long, float, )):
                args[i] = ConstAlgebraicExpression(arg)

        super(AlgebraicEqualityRelation, self).__init__(*args)

    def is_true(self):
        try:
            return self.args[0].eval() == self.args[1].eval()
        except NotImplementedError:
            return False

    @property
    def left_part(self):
        return self.args[0]

    @property
    def right_part(self):
        return self.args[1]

    def __unicode__(self):
        parser = ExpressionParser()
        return '{} = {}'.format(parser.to_unicode(self.left_part), parser.to_unicode(self.right_part))


class CanNotSolve(Exception):
    pass


class LinearEquationSolver(object):
    def __init__(self, equation):
        self.history = [equation]

    def solve(self, variable):
        # variable is Variable instance or FunctionCall instance
        equation = self.history[-1]
        if self.right_part_contains(variable, equation) and not self.left_part_contains(variable, equation):
            equation = self.swap(equation)

        equation = self.simplify_right_part(equation)
        if self.variable_is_isolated(variable, equation):
            return equation.args[1]

        variable_arg_index = self.get_variable_arg_index(variable, equation.left_part)
        for index, arg in enumerate(equation.left_part.args):
            if index == variable_arg_index:
                continue
            equation = self.move_summand_right(index, equation)

        if self.can_unwrap(equation.left_part):
            equation = self.unwrap_left(equation)

        if self.variable_is_isolated(variable, equation):
            return equation.args[1]
        else:
            raise CanNotSolve

    def simplify_right_part(self, equation):
        status, simplified_right_part = self.simplify_expression(equation.right_part)
        if status:
            equation = AlgebraicEqualityRelation(equation.left_part, simplified_right_part)
            self.history.append(equation)

        return equation

    def simplify_expression(self, expression):
        if expression.operator == '+':
            const_expressions = []
            other_expressions = []
            for arg in expression.args:
                if isinstance(arg, ConstAlgebraicExpression):
                    const_expressions.append(arg)
                else:
                    other_expressions.append(arg)
            if len(const_expressions) < 2:
                return False, expression

            zero = const_expressions[0].zero()
            const_expression = sum(const_expressions, zero)

            args = other_expressions
            args.append(const_expression)

            if len(args) == 1:
                return True, args[0]
            else:
                return True, AlgebraicExpression('+', args)
        elif expression.operator == '-':
            if len(expression.args) != 2:
                return False, expression

            const_expressions = []
            for arg in expression.args:
                if isinstance(arg, ConstAlgebraicExpression):
                    const_expressions.append(arg)

            if len(const_expressions) == 2:
                expression = const_expressions[0] - const_expressions[1]
                return True, expression
            else:
                return False, expression
        else:
            return False, expression

    def get_variable_arg_index(self, variable, expression):
        for index, arg in enumerate(expression.args):
            if self.expression_contains(variable, arg):
                return index

    def swap(self, equation):
        left, right = equation.args
        equation = AlgebraicEqualityRelation(right, left)
        self.history.append(equation)
        return equation

    def variable_is_isolated(self, variable, equation):
        return equation.args[0] == variable

    def can_unwrap(self, expression):
        if expression.operator in ('+', '*') and len(expression.args) == 1:
            return True
        else:
            return False

    def unwrap_left(self, equation):
        return AlgebraicEqualityRelation(equation.left_part.args[0], equation.right_part)

    def expression_contains(self, variable, expression):
        if isinstance(expression, Variable):
            return expression == variable
        elif isinstance(expression, ConstAlgebraicExpression):
            return False
        else:
            for arg in expression.args:
                if self.expression_contains(arg, variable):
                    return True
            return False

    def left_part_contains(self, variable, equation):
        return self.expression_contains(variable, equation.left_part)

    def right_part_contains(self, variable, equation):
        return self.expression_contains(variable, equation.right_part)

    def get_sign(self, variable, expression):
        return +1

    def move_summand_right(self, arg_index, equation):
        left_part, right_part = equation.args
        arg = left_part.args.pop(arg_index)
        right_part = AlgebraicExpression('-', [right_part, arg])
        status, right_part = self.simplify_expression(right_part)
        equation = AlgebraicEqualityRelation(left_part, right_part)
        self.history.append(equation)
        return equation


class MeasureUnitConverter(object):
    def __init__(self):
        self.registry = [
            AlgebraicEqualityRelation(MeasureResult(Decimal(1), 'cm'), MeasureResult(Decimal(10), 'mm')),
            AlgebraicEqualityRelation(MeasureResult(Decimal(1), 'm'), MeasureResult(Decimal(10), 'cm')),
        ]

    def build_convertion_rule(self, unit_from, unit_to):
        # what if m -> mm ?
        for rule in self.registry:
            if sets_equal([arg.unit for arg in rule.args], [unit_from, unit_to]):
                if rule.args[0].unit == unit_from:
                    return rule
                else:
                    args = rule.args[::-1]
                    return AlgebraicEqualityRelation(*args)

    def convert(self, measure_result, unit_to):
        rule = self.build_convertion_rule(measure_result.unit, unit_to)
        value_from, value_to = [arg.value for arg in rule.args]
        return MeasureResult(measure_result.value * value_to / value_from, unit_to)
