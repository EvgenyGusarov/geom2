import pickle
from django.contrib import admin
from .models import MathTask, RelationFormTemplate, Problem, Relation, RelationArg
from solver.reasoning import Logger


@admin.register(MathTask)
class MathTaskAdmin(admin.ModelAdmin):
    list_display = ('created', 'get_relations', 'get_to_derive', 'get_to_calculate', )

    def get_relations(self, obj):
        if obj.relations is not None:
            relations = pickle.loads(obj.relations)
            return Logger.to_unicode(relations)
    get_relations.short_description = 'relations'

    def get_to_derive(self, obj):
        if obj.to_derive is not None:
            to_derive = pickle.loads(obj.to_derive)
            return Logger.to_unicode(to_derive)
    get_to_derive.short_description = 'to derive'

    def get_to_calculate(self, obj):
        if obj.to_calculate is not None:
            to_calculate = pickle.loads(obj.to_calculate)
            return Logger.to_unicode(to_calculate)
    get_to_calculate.short_description = 'to calculate'


@admin.register(RelationFormTemplate)
class RelationFormTemplateAdmin(admin.ModelAdmin):
    list_display = ('class_name', 'template', 'description', )
    search_fields = ('class_name', 'description', )


class ProblemRelationTemplateInline(admin.TabularInline):
    model = Relation
    extra = 1


@admin.register(Problem)
class ProblemAdmin(admin.ModelAdmin):
    list_display = ('title', 'created', )
    inlines = (ProblemRelationTemplateInline, )


class RelationArgInline(admin.TabularInline):
    model = RelationArg
    extra = 1


@admin.register(Relation)
class RelationAdmin(admin.ModelAdmin):
    inlines = (RelationArgInline, )
