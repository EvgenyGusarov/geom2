# coding=utf-8

from django.db import models
from django.utils import timezone


class MathTask(models.Model):
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    relations = models.BinaryField(blank=True, null=True)
    to_derive = models.BinaryField(blank=True, null=True)
    to_calculate = models.BinaryField(blank=True, null=True)


class RelationFormTemplate(models.Model):
    class_name = models.CharField(max_length=255)
    template = models.CharField(max_length=255)
    description = models.CharField(max_length=255)

    def __unicode__(self):
        return self.description


class Problem(models.Model):
    title = models.CharField(max_length=255)
    created = models.DateTimeField(default=timezone.now)

    @property
    def relations(self):
        return Relation.objects.filter(problem=self, type=Relation.GIVEN)

    @property
    def relations_to_derive(self):
        return Relation.objects.filter(problem=self, type=Relation.TO_DERIVE)

    def __unicode__(self):
        return self.title


class Relation(models.Model):
    GIVEN = 1
    TO_DERIVE = 2
    RELATION_TYPES = (
        (GIVEN, u'дано'),
        (TO_DERIVE, u'доказать')
    )
    type = models.IntegerField(default=GIVEN, choices=RELATION_TYPES)
    template = models.ForeignKey('RelationFormTemplate')
    problem = models.ForeignKey('Problem', blank=True, null=True)
    position = models.IntegerField(default=0)

    def __unicode__(self):
        # print 'args', [arg.name for arg in self.args.order_by('position')]
        args = [arg.name for arg in self.args.order_by('position')]
        try:
            return u'{}, {}'.format(self.template, self.template.template.format(*args))
        except IndexError:
            return unicode(self.template)


class RelationArg(models.Model):
    name = models.CharField(max_length=255)
    relation = models.ForeignKey('Relation', blank=True, null=True, related_name='args')
    position = models.IntegerField(default=0)
