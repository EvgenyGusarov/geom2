import React from 'react'
import { IndexLink } from 'react-router'

import './style.sass'

export default LayoutBase


const LayoutBase = (props) => (
    <div className="app">
        <div className="app__all">
            <header className="app__header">
                <div className="app__header_logo">
                    <IndexLink to="/">МегаАпп</IndexLink>
                </div>
            </header>
        </div>
        {props.children}
        <footer className="app__footer">
        </footer>
    </div>
);
