import React from 'react'
import ReactDOM from 'react-dom'
import configureStore from './configureStore'

import { App } from './components'

let store = configureStore();


ReactDOM.render(
    <App store={store} />,
    document.getElementById("app")
);
// console.log('');