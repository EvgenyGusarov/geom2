import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import {RECEIVE_MATH_TASKS, RECEIVE_CURRENT_MATH_TASK, RECEIVE_RELATION_TEMPLATES, UPDATE_MATH_TASK,
    APPEND_NEW_RELATION, APPEND_NEW_RELATION_TO_DERIVE, SELECT_RELATION_TEMPLATE, UPDATE_RELATION_FORM,
    UPDATE_RELATION_TO_DERIVE_FORM, CLEAR_CURRENT_MATH_TASK, DELETE_RELATION_FORM, DELETE_RELATION_TO_DERIVE_FORM,
    EDIT_RELATION_FORM, EDIT_RELATION_TO_DERIVE_FORM
} from '../actions'


function math_tasks(state=[], action) {
    switch (action.type) {
        case RECEIVE_MATH_TASKS:
            return action.math_tasks;
        default:
            return state;
    }
}

function current_math_task(state=null, action) {
    switch (action.type) {
        case RECEIVE_CURRENT_MATH_TASK:
            return action.current_math_task;
        case CLEAR_CURRENT_MATH_TASK:
            return {
                relation_forms: [],
                relation_to_derive_forms: []
            };
        case UPDATE_MATH_TASK:
            return Object.assign({...state}, action.fields);
        case APPEND_NEW_RELATION:
            // state.relation_forms = state.relation_forms.concat(action.relation_form);
            return {
                ...state,
                relation_forms: [...state.relation_forms, action.relation_form]
            };
        case APPEND_NEW_RELATION_TO_DERIVE:
            return {
                ...state,
                relation_to_derive_forms: [...state.relation_to_derive_forms, action.relation_form]
            };
        case UPDATE_RELATION_FORM:
            return {
                ...state,
                relation_forms: [
                    ...state.relation_forms.slice(0, action.relation_index),
                    {relation: action.relation, action: action.action},
                    ...state.relation_forms.slice(action.relation_index + 1)
                ]
            };
        case UPDATE_RELATION_TO_DERIVE_FORM:
            return {
                ...state,
                relation_to_derive_forms: [
                    ...state.relation_to_derive_forms.slice(0, action.relation_index),
                    {relation: action.relation, action: action.action},
                    ...state.relation_to_derive_forms.slice(action.relation_index + 1)
                ]
            };
        case DELETE_RELATION_FORM:
            state.relation_forms.splice(action.relation_index, 1);
            return {
                ...state
            };
        case DELETE_RELATION_TO_DERIVE_FORM:
            state.relation_to_derive_forms.splice(action.relation_index, 1);
            return {
                ...state
            };
        case EDIT_RELATION_FORM:
            state.relation_forms[action.relation_index].action = 'edit';
            return {
                ...state
            };
        case EDIT_RELATION_TO_DERIVE_FORM:
            state.relation_to_derive_forms[action.relation_index].action = 'edit';
            return {
                ...state
            };
        default:
            return state;
    }
}


function relation_templates(state=[], action) {
    switch (action.type) {
        case RECEIVE_RELATION_TEMPLATES:
            return action.relation_templates;
        default:
            return state;
    }
}


const rootReducer = combineReducers({
    math_tasks,
    current_math_task,
    relation_templates,
    form: formReducer
});

export default rootReducer