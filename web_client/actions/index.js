export const REQUEST_MATH_TASKS = 'REQUEST_MATH_TASKS';

export function request_math_tasks() {
    return {
        type: REQUEST_TASKS
    }
}


export const RECEIVE_MATH_TASKS = 'RECEIVE_MATH_TASKS';

export function receiveMathTasks(json) {
  return {
    type: RECEIVE_MATH_TASKS,
    math_tasks: json
  }
}


export const RECEIVE_RELATION_TEMPLATES = 'RECEIVE_RELATION_TEMPLATES';

export function receiveRelationTemplates(json) {
    return {
        type: RECEIVE_RELATION_TEMPLATES,
        relation_templates: json
    }
}


export const RECEIVE_CURRENT_MATH_TASK = 'RECEIVE_CURRENT_MATH_TASK';

export function receiveCurrentMathTask(json) {
    return {
        type: RECEIVE_CURRENT_MATH_TASK,
        current_math_task: json
    }
}


export const CLEAR_CURRENT_MATH_TASK = 'CLEAR_CURRENT_MATH_TASK';

export function clearCurrentMathTask() {
    return {
        type: CLEAR_CURRENT_MATH_TASK
    }
}


export const UPDATE_MATH_TASK = 'UPDATE_MATH_TASK';

export function updateMathTask(update_fields) {
    return {
        type: UPDATE_MATH_TASK,
        fields: update_fields
    }
}


export const APPEND_NEW_RELATION = 'APPEND_NEW_RELATION';

export function appendNewRelation(relation_form) {
    return {
        type: APPEND_NEW_RELATION,
        relation_form: relation_form
    }
}

export const APPEND_NEW_RELATION_TO_DERIVE = 'APPEND_NEW_RELATION_TO_DERIVE';

export function appendNewRelationToDerive(relation_form) {
    return {
        type: APPEND_NEW_RELATION_TO_DERIVE,
        relation_form: relation_form
    }
}


export const UPDATE_RELATION_FORM = 'UPDATE_RELATION_FORM';

export function updateRelationForm(relation_index, relation, action) {
    return {
        type: UPDATE_RELATION_FORM,
        relation_index, relation, action
    }
}


export const UPDATE_RELATION_TO_DERIVE_FORM = 'UPDATE_RELATION_TO_DERIVE_FORM';

export function updateRelationToDeriveForm(relation_index, relation, action) {
    return {
        type: UPDATE_RELATION_TO_DERIVE_FORM,
        relation_index, relation, action
    }
}


export const DELETE_RELATION_FORM = 'DELETE_RELATION_FORM';

export function deleteRelationForm(relation_index) {
    return {
        type: DELETE_RELATION_FORM,
        relation_index
    }
}


export const DELETE_RELATION_TO_DERIVE_FORM = 'DELETE_RELATION_TO_DERIVE_FORM';

export function deleteRelationToDeriveForm(relation_index) {
    return {
        type: DELETE_RELATION_TO_DERIVE_FORM,
        relation_index
    }
}


export const EDIT_RELATION_FORM = 'EDIT_RELATION_FORM';

export function editRelationForm(relation_index) {
    return {
        type: EDIT_RELATION_FORM,
        relation_index
    }
}


export const EDIT_RELATION_TO_DERIVE_FORM = 'EDIT_RELATION_FORM';

export function editRelationToDeriveForm(relation_index) {
    return {
        type: EDIT_RELATION_TO_DERIVE_FORM,
        relation_index
    }
}