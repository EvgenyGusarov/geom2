import React from 'react'
import { Link } from 'react-router-dom'
import { format_date } from '../../utils'


export default ({ math_task }) => (
    <div>
        <h4>
            <Link to={`/task/${math_task.id}/`}>{ math_task.title }</Link>
        </h4>
        <p className="time">{ format_date(math_task.created) }</p>
    </div>
)