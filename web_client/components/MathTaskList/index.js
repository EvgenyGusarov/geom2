import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import MathTaskListItem from '../MathTaskListItem'
import {receiveMathTasks, clearCurrentMathTask} from '../../actions'

import '!style-loader!css-loader!bootstrap/dist/css/bootstrap.css'
import './style.sass'


class MathTaskList extends Component {
    async loadMathTasks() {
        const {dispatch} = this.props;
        await fetch("/api/v0/task/").then(response => response.json()).then(json => dispatch(receiveMathTasks(json)));
    }

    clearCurrentMathTask() {
        const {dispatch} = this.props;
        dispatch(clearCurrentMathTask());
    }

    componentDidMount() {
        this.loadMathTasks();
        this.clearCurrentMathTask();
    }

    render() {
        console.log('MathTaskList.render');
        console.log('this.props');
        console.log(this.props);
        return(
            <ul className="content-list list-unstyled">
                {this.props.math_tasks.map((math_task, index) => (
                    <li className="content-list__item" key={index}>
                        <MathTaskListItem math_task={math_task} />
                    </li>
                ))}
                <li className="content-list__item" key="new">
                    <Link to="/task/new/" className="btn btn-primary">Добавить задачу</Link>
                </li>
            </ul>
        );
    }
}

MathTaskList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

function mapStateToProps(state) {
    const {math_tasks} = state;
    return {math_tasks};
}

export default connect(mapStateToProps)(MathTaskList)