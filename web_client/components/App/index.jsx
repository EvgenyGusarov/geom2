import React, { Component } from 'react'
import {
    BrowserRouter,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom'
import { Provider } from 'react-redux'
import PropTypes from 'prop-types'

import { BaseLayout } from '../../layouts'
import MathTaskList from '../MathTaskList'
import MathTask from '../MathTask'

// import 'bootstrap-sass'


const App = ({ store }) => (
    <Provider store={store}>
        <BrowserRouter>
            <div className="container">
                {/*<Redirect exact={true} from="/" to="/task/list/" />*/}
                {/*<Route exact={true} path="/task/list/" component={ BaseLayout } >*/}
                    <Switch>
                        <Route path="/task/list/" component={MathTaskList} />
                        <Route path="/task/new/" component={MathTask} />
                        <Route path="/task/:task_id/" component={MathTask} />
                    </Switch>
                {/*</Route>*/}
            </div>
        </BrowserRouter>
    </Provider>
);

App.propTypes = {
  store: PropTypes.object.isRequired
};


// class App extends Component {
//     render(){
//         return(
//
//         );
//     }
// }

export default App