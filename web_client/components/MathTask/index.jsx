import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
// import { Grid, Row, Col } from 'react-bootstrap'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
import Col from 'react-bootstrap/lib/Col'
import Button from 'react-bootstrap/lib/Button'
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar'
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger'
import Tooltip from 'react-bootstrap/lib/Tooltip'

import Relation from '../Relation'
import {RELATION_TYPES} from '../Relation'
import { receiveRelationTemplates, receiveCurrentMathTask, appendNewRelation, appendNewRelationToDerive,
    updateRelationForm, clearCurrentMathTask,
    updateMathTask}
    from '../../actions'

import '!style-loader!css-loader!bootstrap/dist/css/bootstrap.css'
import './style.sass'


class MathTask extends Component {
    constructor(props) {
        super(props);

        this.updateMathTaskTitle = this.updateMathTaskTitle.bind(this);
        this.appendNewRelation = this.appendNewRelation.bind(this);
        this.appendNewRelationToDerive = this.appendNewRelationToDerive.bind(this);
        this.submit = this.submit.bind(this);
    }

    async loadMathTask(task_id) {
        // console.log('MathTask.loadMathTask');

        const {dispatch} = this.props;
        await fetch(`/api/v0/task/${task_id}/`).
            then(response => response.json()).
            then(this.simplify_args).
            then(this.assign_default_actions).
            then(json => dispatch(receiveCurrentMathTask(json)));
    }

    initMathTask() {
        const {dispatch} = this.props;
        dispatch(clearCurrentMathTask());
        dispatch(
            appendNewRelation(
                {action: 'edit'}
            )
        );
    }

    assign_default_actions(math_task) {
        // console.log('math_task');
        // console.log(math_task);
        math_task.relation_forms = [];
        math_task.relation_to_derive_forms = [];
        math_task.relations.forEach((relation) => math_task.relation_forms.push({
            relation: relation,
            action: "read"
        }));
        math_task.relations_to_derive.forEach((relation) => math_task.relation_to_derive_forms.push({
            relation: relation,
            action: "read"
        }));
        delete math_task.relations;
        delete math_task.relations_to_derive;
        return math_task;
    }

    simplify_args(math_task) {
        math_task.relations.forEach(relation => (
            relation.args = relation.args.map(arg => arg.name)
        ));
        math_task.relations_to_derive.forEach(relation => (
            relation.args = relation.args.map(arg => arg.name)
        ));
        return math_task;
    }

    extract_relations() {
        let relations = [];
        for (let relation_form of this.props.current_math_task.relation_forms) {
            const relation = relation_form.relation;
            relations.push({
                // type: RELATION_TYPES.GIVEN,
                template: relation.template,
                args: relation.args.map(
                    (name, position) => ({name, position})
                )
            });
        }
        return relations;
    }

    extract_relations_to_derive() {
        let relations_to_derive = [];
        for (let relation_form of this.props.current_math_task.relation_to_derive_forms) {
            const relation = relation_form.relation;
            relations_to_derive.push({
                // type: RELATION_TYPES.TO_DERIVE,
                template: relation.template,
                args: relation.args.map(
                    (name, position) => ({name, position})
                )
            });
        }
        return relations_to_derive;
    }

    async loadRelationTemplates() {
        // console.log('MathTask.loadRelationTemplates');
        const {dispatch} = this.props;
        await fetch("/api/v0/task/relation-template/").
            then(response => response.json()).
            then(json => dispatch(receiveRelationTemplates(json)));
    }

    componentDidMount() {
        // console.log('componentDidMount');
        this.loadRelationTemplates();

        const task_id = this.props.match.params.task_id;
        console.log('task_id ' + task_id);
        if (task_id) {
            this.loadMathTask(task_id);
        } else {
            this.initMathTask();
        }
    }

    appendNewRelation() {
        const {dispatch} = this.props;
        dispatch(appendNewRelation({action: "edit"}));
    }

    appendNewRelationToDerive() {
        const {dispatch} = this.props;
        dispatch(appendNewRelationToDerive({action: "edit"}));
    }

    updateMathTaskTitle(event) {
        const {dispatch} = this.props;
        dispatch(
            updateMathTask({ title: event.target.value})
        );
    }
    
    submit() {
        const task_id = this.props.match.params.task_id;
        const url = task_id ? `/api/v0/task/${task_id}/` : '/api/v0/task/new/';
        const method = task_id ? 'put' : 'post';

        let body = {
            title: this.props.current_math_task.title,
            relations: this.extract_relations(),
            relations_to_derive: this.extract_relations_to_derive()
        };

        const {dispatch} = this.props;

        fetch(url, {
            method: method,
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(body)
        }).
        then(response => response.json()).
        then(this.simplify_args).
        then(this.assign_default_actions).
        then(math_task => dispatch(receiveCurrentMathTask(math_task)));
    }

    render() {
        console.log('MathTask.render');
        console.log(this.props);

        if (!this.props.current_math_task) {
            return <div></div>
        }

        const add_relation_tooltip = (
          <Tooltip id="add_relation_tooltip"><strong>Добавить утверждение</strong></Tooltip>
        );

        return(
            <div>
                <label key="title">
                    <input type="text" name="title" value={this.props.current_math_task.title || ''}
                           placeholder="Новая задача"
                           onChange={this.updateMathTaskTitle}/>
                </label>
                <Row className="show-grid">
                    <Col md={4}>
                        <h4>Что дано:</h4>
                        <ul className="content-list list-unstyled">
                            {this.props.current_math_task.relation_forms.map((relation_form, index) => (
                                <li className="content-list__item" key={index}>
                                    <Relation relation={relation_form.relation} action={relation_form.action}
                                              relation_templates={this.props.relation_templates}
                                              position={index} relation_type={RELATION_TYPES.GIVEN}
                                    />
                                </li>
                            ))}
                        </ul>
                        <div className="card-footer">
                            <OverlayTrigger placement="right" overlay={add_relation_tooltip}>
                                <Button className="glyphicon glyphicon-plus" onClick={this.appendNewRelation}></Button>
                            </OverlayTrigger>
                        </div>
                        {/*<span onClick={this.appendNewRelation}>Добавить утверждение</span>*/}
                    </Col>
                    <Col md={4}>
                        <h4>Что доказываем:</h4>
                        <ul className="content-list list-unstyled">
                            {this.props.current_math_task.relation_to_derive_forms.map((relation_form, index) => (
                                <li className="content-list__item" key={index}>
                                    <Relation relation={relation_form.relation} action={relation_form.action}
                                              relation_templates={this.props.relation_templates}
                                              position={index} relation_type={RELATION_TYPES.TO_DERIVE}
                                    />
                                </li>
                            ))}
                        </ul>
                        <div className="card-footer">
                            <OverlayTrigger placement="right" overlay={add_relation_tooltip}>
                                <Button className="glyphicon glyphicon-plus" onClick={this.appendNewRelationToDerive}></Button>
                            </OverlayTrigger>
                        </div>
                    </Col>
                </Row>
                <br/>
                <ButtonToolbar>
                    <Button onClick={this.submit} className="btn btn-primary" type="submit">Сохранить</Button>
                    <Link to="/task/list/" className="btn btn-default" role="button">Back to list</Link>
                </ButtonToolbar>
            </div>
        );
    }
}

MathTask.propTypes = {
    dispatch: PropTypes.func.isRequired,
    // current_math_task: PropTypes.node.isRequired,
    // relation_templates: PropTypes.node.isRequired,
};

function mapStateToProps(state) {
    const {current_math_task, relation_templates} = state;

    return {current_math_task, relation_templates};
}

// function mapDispatchToProps(dispatch) {
//     return {dispatch};
// }

export default connect(mapStateToProps)(MathTask)