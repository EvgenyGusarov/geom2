import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Link } from 'react-router-dom'
import { format_date } from '../../utils'
import { updateRelationForm, updateRelationToDeriveForm, deleteRelationForm, deleteRelationToDeriveForm,
    editRelationForm, editRelationToDeriveForm}
    from '../../actions'

import '!style-loader!css-loader!sass-loader!./style.sass'


export const RELATION_TYPES = {
    GIVEN: 1,
    TO_DERIVE: 2
};


class Relation extends Component {
    state = {
        relation_template: '',
        labels: [],
        inputs: []
    };

    constructor(props) {
        super(props);

        this.deleteRelation = this.deleteRelation.bind(this);
        this.editRelation = this.editRelation.bind(this);
    }

    componentDidMount() {
        // console.log('Relation.componentDidMount');
    }

    getCurrentTemplate() {
        return this.props.relation ? this.getRelationTemplate(this.props.relation.template) : null;
    }

    getLabels() {
        const current_template = this.getCurrentTemplate();
        console.log(current_template);
        return current_template ? current_template.template.split('{}') : [];
    }

    getInputs() {
        return this.props.relation ? this.props.relation.args : [];
    }

    getSelectValue() {
        const current_template = this.getCurrentTemplate();
        return current_template ? current_template.id : '';
    }

    renderSelect() {
        const select_value = this.getSelectValue();

        return(
            <select name="relation_template" value={select_value}
                    onChange={this.handleSelectChangeFactory(this.props.relation_type, this.props.position)}>
                <option key="default" value="">Выбери шаблон из списка...</option>
                {this.props.relation_templates.map((relation_template, index) => (
                    <option key={index} value={relation_template.id}>
                        {relation_template.description}
                    </option>
                ))}
            </select>
        );
    }

    getRelationTemplate(relation_template_id) {
        for (let relation_template of this.props.relation_templates) {
            if (relation_template.id == relation_template_id) {
                return relation_template;
            }
        }
    }

    handleSelectChangeFactory(relation_type, relation_index) {
        return this.handleSelectChange.bind(this, relation_type, relation_index);
    }

    handleTextInputChangeFactory(relation_type, relation_index, input_index) {
        return this.handleTextInputChange.bind(this, relation_type, relation_index, input_index);
    }

    handleSelectChange(relation_type, relation_index, event) {
        console.log('handleSelectChange ' + relation_index);

        const relation_template_id = event.target.value;
        const relation_template = this.getRelationTemplate(relation_template_id);
        let relation = null;

        if (relation_template) {
            const labels = relation_template.template.split('{}');
            relation = {
                template: relation_template_id,
                args: Array(labels.length - 1).fill('')
            };
        }

        const {dispatch} = this.props;

        switch (relation_type) {
            case RELATION_TYPES.GIVEN:
                dispatch(
                    updateRelationForm(relation_index, relation, 'edit')
                );
                break;
            case RELATION_TYPES.TO_DERIVE:
                dispatch(
                    updateRelationToDeriveForm(relation_index, relation, 'edit')
                );
                break;
            default:
                break;
        }

    }

    handleTextInputChange(relation_type, relation_index, input_index, event) {
        console.log(`MathTask.handleTextInputChange(${relation_index}, ${input_index}, ${event.target.value})`);

        const args = this.props.relation.args;

        let relation = {
            ...this.props.relation,
            args: [
                ...args.slice(0, input_index),
                event.target.value,
                ...args.slice(input_index + 1)
            ]
        };

        const {dispatch} = this.props;

        switch (relation_type) {
            case RELATION_TYPES.GIVEN:
                dispatch(
                    updateRelationForm(relation_index, relation, 'edit')
                );
                break;
            case RELATION_TYPES.TO_DERIVE:
                dispatch(
                    updateRelationToDeriveForm(relation_index, relation, 'edit')
                );
                break;
            default:
                break;
        }
    }

    deleteRelation() {
        const {dispatch} = this.props;
        switch (this.props.relation_type) {
            case RELATION_TYPES.GIVEN:
                dispatch(deleteRelationForm(this.props.position));
                break;
            case RELATION_TYPES.TO_DERIVE:
                dispatch(deleteRelationToDeriveForm(this.props.position));
                break;
            default:
                break;
        }
    }

    editRelation() {
        const {dispatch} = this.props;
        switch (this.props.relation_type) {
            case RELATION_TYPES.GIVEN:
                dispatch(editRelationForm(this.props.position));
                break;
            case RELATION_TYPES.TO_DERIVE:
                dispatch(editRelationToDeriveForm(this.props.position));
                break;
            default:
                break;
        }
    }

    renderTextInputs() {
        const current_template = this.getCurrentTemplate();
        if (!current_template) return '';

        const labels = this.getLabels();
        const inputs = this.getInputs();

        let chunks = [];
        const last_index = labels.length - 1;

        for (let input_index = 0; input_index < labels.length - 1; input_index++) {
            chunks.push(
                    <label key={'label_' + input_index}>
                        {labels[input_index]}
                    </label>
                        ,
                    <input type="text" key={'input_' + input_index} name={'input_' + input_index} value={inputs[input_index]} size="5"
                        onChange={this.handleTextInputChangeFactory(this.props.relation_type, this.props.position, input_index)}
                   />
            );
        }
        chunks.push(
            <label key={'label_' + last_index}>{labels[last_index]}</label>
        );

        return (
            <div className="row">
                <div className="col-md-9">
                    {chunks}
                </div>
                <div className="col-md-3">
                    <span className="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    &nbsp;&nbsp;
                    <span className="glyphicon glyphicon-remove" aria-hidden="true" onClick={this.deleteRelation}></span>
                </div>
            </div>
        )
    }

    renderAsString() {
        let chunks = [];

        const labels = this.getLabels();
        const inputs = this.getInputs();

        labels.forEach(function (label, index) {
            chunks.push(label);
            if (index < inputs.length) {
                chunks.push(inputs[index]);
            }
        });
        return (
            <div className="row">
                <div className="col-md-9">
                    {chunks}
                </div>
                <div className="col-md-3">
                    <span className="glyphicon glyphicon-pencil" aria-hidden="true" onClick={this.editRelation}></span>
                    &nbsp;&nbsp;
                    <span className="glyphicon glyphicon-remove" aria-hidden="true" onClick={this.deleteRelation}></span>
                </div>
            </div>
        )
    }

    render() {
        console.log('Relation.render');

        switch (this.props.action) {
            case ('read'):
                return this.renderAsString();
            case ('edit'):
                return (
                    <div>
                        <form>
                            {this.renderSelect()}
                            {this.renderTextInputs()}
                            {/*<button type="submit">Выбрать</button>*/}
                        </form>
                    </div>
                );
            default:
                console.log('no action');
                return;
        }
    }
}

Relation.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

export default connect()(Relation);